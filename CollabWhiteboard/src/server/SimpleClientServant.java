package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;

import server.exceptions.LoginException;
import server.exceptions.ServantException;
import client.BoardImage;
import client.BoardModification;
import client.ClientMessenger;


public class SimpleClientServant extends UnicastRemoteObject
    implements ClientServant
{

    private static final long serialVersionUID = -1816257953686251149L;
    private CentralWhiteboard centralWhiteboard;
    private String clientname;

    //the ClientMessenger used for reverse communication to client
    //keep a reference to it so that it will not be garbage collected
    public ClientMessenger messenger;

    public SimpleClientServant(String username,
                CentralWhiteboard centralWhiteboard, ClientMessenger messenger)
            throws RemoteException, LoginException
    {
        super();
        this.clientname = username;
        this.centralWhiteboard = centralWhiteboard;
        this.messenger = messenger;
        centralWhiteboard.registerClientMessenger(clientname, messenger, this);
    }

    /*
     * call attemptUpdate from the CentralWhiteboard object, and return the
     * result to client (success or failure)
     */
    public boolean updateBoard(BoardModification modification)
            throws RemoteException, ServantException
    {
        return centralWhiteboard.attemptUpdate(clientname, modification);
    }

    /*
     * poke the central whiteboard
     */
    public void pokeServer()
            throws RemoteException
    {
        centralWhiteboard.poke(clientname);
    }

    /*
     * indicates that client wishes to disconnect from server;
     * centralWhiteboard will remove references to this client and alert
     * other clients that they have logged off
     */
    public void logout() throws RemoteException
    {
        centralWhiteboard.logout(clientname);
    }

    /*
     * just calls corresponding method on central whiteboard
     */
    public void kickClient(String clientToKick)
            throws RemoteException, ServantException
    {
        centralWhiteboard.kickPeer(clientname, clientToKick);
    }

    /*
     * just calls corresponding method on central whiteboard
     */
    public void promoteClient(String clientToPromote)
            throws RemoteException, ServantException
    {
        centralWhiteboard.promotePeer(clientname, clientToPromote);
    }
    
    /*
     * delegates method call to the central whiteboard
     */
    public void setBoardImage(BoardImage newimage) throws RemoteException,
            ServantException
    {
        centralWhiteboard.setBoardImage(clientname, newimage);
    }

    /*
     * delegates method call to the cental whiteboard
     */
    public LinkedList<String> getClientList()
            throws RemoteException
    {
        return centralWhiteboard.getClientList();
    }

    /*
     * delegates method call to the central whiteboard
     */
    public String getAdminName()
            throws RemoteException
    {
        return centralWhiteboard.getAdminName();
    }

    /*
     * delegates method call to the central whiteboard
     */
    public BoardImage getBoardImage()
            throws RemoteException
    {
        return centralWhiteboard.getBoardImage(clientname);
    }





}
