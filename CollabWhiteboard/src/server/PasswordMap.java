
package server;

import java.util.ArrayList;
import java.io.*;

import server.exceptions.*;


/**
 *  A class to provide an abstract mapping from a username to a password
 *  that is stored on the server. This class implements that mapping by
 *  keeping a set of files, one per user, that store the password for the
 *  corresponding user. The files use names of the form [username].passwd.
 */
public class PasswordMap
{
    private static final char[] ILLEGAL_FILENAME_CHARS = { '/', '\n', '\r', '\t',
        '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':', '.' };

    private String prefix;

    /**
     *  Initialise the password mapper. This method is given the path to
     *  the directory where password files are stored as a parameter.
     *  The prefix must end with a '/' pathname separator.
     */
        public
    PasswordMap (String pathPrefix)
    {
        this.prefix = pathPrefix + "/";
    }

    /**
     *  Return the stored password as an array of chars. Using char arrays
     *  ensures that the password doesn't go into the java string pool,
     *  which would be a security concern.
     *
     *  @throws AuthenticationException if no such user exists, or if the
     *      username contains illegal characters.
     */
        public char []
    getPasswordForUser (String username) throws AuthenticationException
    {
        if (checkForIllegalChars (username))
            throw new AuthenticationException ();

        String filename = prefix + username + ".pass";
        return readPasswordFromFile (filename);
    }

    /**
     *  Create a password file for a new user account.
     *
     *  @throws CreateAccountException if the username is already in use.
     */
        public void
    storePasswordForNewUser (String username, char [] password)
        throws CreateAccountException
    {
        String filename;

        if (checkForIllegalChars (username))
            throw new CreateAccountException ("That username contains " +
                    "characters that aren't allowed");

        filename = prefix + username + ".passwd";

        if (isExistingUser (username))
        {
            throw new CreateAccountException ("I can't create an " +
                    "account for " + username + ". Someone else has " +
                    "already acquired that username.");
        }

        writePasswordFile (filename, password);
    }

        private boolean
    isExistingUser (String username)
    {
        boolean result;

        try
        {
            // if we can get a password, the user account must exist.
            getPasswordForUser (username);
            result = true;
        }
        catch (AuthenticationException e)
        {
            result = false;
        }

        return result;
    }

    /**
     *  Checks that the username does not contain any chars that have
     *  reserved meanings for the filesystem. '.', '..' or pathname
     *  separators, for example, could potentially be used for a security
     *  exploit if they were permitted to occurr in a username.
     *
     *  @return true if the username contains illegal chars.
     */
        private boolean
    checkForIllegalChars (String filename)
    {
        boolean result = false;

        for (char c : filename.toCharArray ())
        {
            for (char illegalChar : ILLEGAL_FILENAME_CHARS)
            {
                if (c == illegalChar)
                    result = true;
            }
        }

        return result;
    }

        private char []
    readPasswordFromFile (String filename) throws AuthenticationException
    {
        BufferedReader file;
        ArrayList <Character> password;

        try
        {
            file = new BufferedReader (new FileReader (filename));
            password = getLineAsCharArray (file);
        }
        catch (Exception e)
        {
            System.out.println ("Exception while reading password");
            e.printStackTrace ();
            throw new AuthenticationException ();
        }

        return getArrayFromArrayList (password);
    }

    /**
     *  write a new password to a specified file. The file is created if
     *  it does not exist (typical case).
     */
        private void
    writePasswordFile (String filename, char [] password)
        throws CreateAccountException
    {
        FileOutputStream fd;
        DataOutputStream file;

        try
        {
            fd = new FileOutputStream (filename);
            file = new DataOutputStream (fd);

            for (char c : password)
                file.writeChar ((int) c);

            fd.close ();
        }
        catch (Exception e)
        {
            throw new CreateAccountException ("Uh oh. I couldn't write " +
                    "the new password. I can't help you with this, you " +
                    "will have to be like Hercule Poirot, and " +
                    "isolate the problem using the little grey cells ;-)");
        }
    }

        private ArrayList <Character>
    getLineAsCharArray (BufferedReader file) throws IOException
    {
        ArrayList <Character> result = new ArrayList <Character> ();
        char nextChar;

        while (true)
        {
            nextChar = (char) file.read ();

            if ((nextChar == '\n') || (nextChar == '\r') ||
                    (nextChar == -1))
            {
                break;
            }
            else
            {
                result.add (nextChar);
            }
        }

        return result;
    }

        private char []
    getArrayFromArrayList (ArrayList <Character> toConvert)
    {
        char [] result = new char [toConvert.size ()];

        for (int i = 0; i < toConvert.size (); i ++)
        {
            result [i] = toConvert.get (i).charValue ();
        }

        return result;
    }
}

// vim: ts=4 sw=4 et
