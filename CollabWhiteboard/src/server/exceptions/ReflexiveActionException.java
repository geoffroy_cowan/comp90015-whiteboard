package server.exceptions;

/*
 * exception that is performed if a client attempts to perform an action
 * that requires a target client on themselves that does not make sense
 * (e.g. kick or promote)
 */
public class ReflexiveActionException extends ServantException
{

    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -5715955280338188785L;

    public ReflexiveActionException(String actionName)
    {
        super("Error: cannot perform " + actionName + " action on self");
    }
  
}
