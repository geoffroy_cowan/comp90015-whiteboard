package server.exceptions;

/*
 * LoginException that is called when a client tries to login to a whiteboard
 * instance but request is denied by the current administrator
 */
public class LoginPermissionDeniedException extends LoginException
{
    
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -2935949069473580208L;

    public LoginPermissionDeniedException
            (String adminName, String whiteboardName)
    {
        super("Error: Login permission to whiteboard instance "
                + whiteboardName + " denied by administrator " + adminName);
    }
}
