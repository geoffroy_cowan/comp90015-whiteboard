package server.exceptions;

/*
 * ServantException that is called if client tries to call a ClientServant
 * method referring to another client which does not exist
 */
public class ClientDoesNotExistException extends ServantException
{

    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -1271335067635427844L;

    public ClientDoesNotExistException(String clientName, String whiteboardName)
    {
        super("Error: there is no client with name " + clientName
                + " connected to whiteboard instance " + whiteboardName);
    }
}
