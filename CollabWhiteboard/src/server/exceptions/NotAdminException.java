package server.exceptions;

/*
 * Exception that is thrown if user attempts to take an action on whiteboard
 * instance that requires administrator privelages
 */
public class NotAdminException extends ServantException
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -1400021629179285730L;
    
    public NotAdminException(String currentAdmin)
    {
        super("Error: administrator privaleges required for action. "
                + "Current administrator is " + currentAdmin);
    }

}
