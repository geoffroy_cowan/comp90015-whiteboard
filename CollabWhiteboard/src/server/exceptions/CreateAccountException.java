package server.exceptions;

/*
 * Exception that can be thrown when creating a new user account
 */
public class CreateAccountException extends Exception
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = 6078216576722809136L;
    
    //basic constructor
    public CreateAccountException(String message)
    {
        super("Error: " + message);
    }
        
}
