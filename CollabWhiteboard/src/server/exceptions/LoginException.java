package server.exceptions;

/*
 * Exception that occurs if there is any error in logging in to server
 * (either for logging into a new or an existing server)
 */
public class LoginException extends Exception
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -9103948764037270656L;

    /*
     * constructor takes a string error message, and calls super constructor
     * with it
     */
    public LoginException(String errorMessage)
    {
        super(errorMessage);
    }
}
