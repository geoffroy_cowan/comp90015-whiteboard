package server.exceptions;

/*
 * exception that is called if we attempt to login to a whiteboard
 * instance with a name that does not exist (or was closed during process
 * of trying to connect)
 */
public class BoardNotFoundException extends LoginException
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -4740829422039804133L;

    public BoardNotFoundException(String whiteboardName)
    {
        super("Error: there is no whiteboard instance with name "
                + whiteboardName + " on server");
    }
}
