package server.exceptions;

/*
 * Exception that is raised if we attempt to obtain BoardImage when there
 * are no active peers to obtain from
 */
public class NoActivePeerException extends ServantException
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -2996057804101978440L;

    public NoActivePeerException()
    {
        super("Error: No active peers on current whiteboard instance");
    }

}
