package server.exceptions;


public class AccountDetailsTooLongException extends CreateAccountException
{

    //generated automatically by eclipse
    private static final long serialVersionUID = 4447934422671912694L;

    public AccountDetailsTooLongException()
    {
        super("Account details too long");
    }

}
