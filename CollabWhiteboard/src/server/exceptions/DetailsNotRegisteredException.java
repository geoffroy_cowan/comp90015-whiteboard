package server.exceptions;

/*
 * exception is thrown if user tries to login using loginTo... methods without
 * first registering their username/password and their ClientReceiver
 */
public class DetailsNotRegisteredException extends LoginException
{
    
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = 5918133920939121232L;

    public DetailsNotRegisteredException()
    {
        super("Error: Login details not registered!");
    }
}
