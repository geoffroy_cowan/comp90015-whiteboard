package server.exceptions;

/*
 * class of exceptions that can be thrown when attempting to call a remote
 * method on server using a ClientServant
 */
public class ServantException extends Exception
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = 1495857977138692969L;
    
    public ServantException(String errorMessage)
    {
        super(errorMessage);
    }
}
