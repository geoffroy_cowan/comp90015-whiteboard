package server.exceptions;

/*
 * exception that is called if we attempt to create a new board
 * instance with a name that already exists
 */
public class BoardNameExistsException extends LoginException
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = -10617056960715877L;

    public BoardNameExistsException(String whiteboardName)
    {
        super("Error: there already exists a whiteboard "
                + "instance with name " + whiteboardName + " on server");
    }
}
