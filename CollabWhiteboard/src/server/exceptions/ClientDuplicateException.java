package server.exceptions;

public class ClientDuplicateException extends LoginException
{
    //needed for serializable class (pseudo-random number picked by Eclipse)
    private static final long serialVersionUID = 2654708167245753783L;

    public ClientDuplicateException(String clientname, String whiteboardName)
    {
        super("Error: there is already a client with username "
                + clientname + " logged into the whiteboard instance "
                + whiteboardName);
    }
}
