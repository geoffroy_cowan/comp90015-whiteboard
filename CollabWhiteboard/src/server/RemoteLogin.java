package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.PublicKey;
import java.util.LinkedList;

import client.*;
import server.exceptions.CreateAccountException;
import server.exceptions.LoginException;


/*
 * an interface for the Remote object that should be exported from
 * server by client in order to handle basic, pre login requests
 * 
 * should allow client to login to a new or existing whiteboard instance,
 * to list current instances, and to create a new user account
 */
public interface RemoteLogin extends Remote
{
    /*
     * Returns a cryptographic salt for the client to use to hash their
     * password.
     */
    public CryptographicSalt getCurrentSalt ()
            throws RemoteException;

    /*
     * Creates a new whiteboard instance, and logs into it as the
     * administrator, using supplied login credentials (username and
     * password), a name for the new instance (whiteboardName),
     * an initial BoardImage to load (startingImage), and a reference
     * to the client's remote ClientMessenger object
     * 
     * note: the supplied whiteboard name must be new, otherwise we will
     * return error
     * 
     * returns a reference to remote ClientServant object client can use
     * to communicate with the whiteboard instance, or throws an exception
     * in case of failure (RemoteException for technical failure,
     * LoginException otherwise (e.g. credentials bad, tried to create
     * new instance with name that already exists, etc)
     */
    public ClientServant
            loginToNew(ClientCredentials credentials,
                    String whiteboardName, ClientMessenger messenger,
                    BoardImage startingImage)
            throws RemoteException, LoginException;
    
    /*
     * Similar to other login method (loginToNew), but instead logs in
     * as a regular client (non administrator) to an existing whiteboard
     * instance (with name whiteboardName)
     * 
     * note: a whiteboard instance with this name must actually exist or
     * an error will occur
     * 
     * arguments have same interpretation as the other method, except
     * startingImage which is obviously not needed in this case
     * 
     * same return/throw behavior as in loginToNew
     *
     */
    public ClientServant
            loginToExisting(ClientCredentials credentials,
                    String whiteboardName, ClientMessenger messenger)
            throws RemoteException, LoginException;
    
    /*
     * list names of all current whiteboard instances
     * 
     * returns linked-List of strings containing the names
     */
    public LinkedList<String> listWhiteboards()
            throws RemoteException;
    
    /*
     * setup new user account given username and password
     * 
     * credentials encrypted according to available public key, using
     * agreed upon encryption algorithm
     * 
     * CreateAccoutnException thrown if account creation failed for
     * any reason
     */
    public void
            createAccount(byte [] encryptedCredentials)
            throws RemoteException, CreateAccountException;
    
    /*
     * obtains the public key for encrypting account information to be sent
     * to server
     */
    public PublicKey getPublicKey() throws RemoteException;
}
