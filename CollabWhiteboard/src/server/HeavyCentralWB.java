package server;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedList;

import server.exceptions.BoardNotFoundException;
import server.exceptions.ClientDoesNotExistException;
import server.exceptions.ClientDuplicateException;
import server.exceptions.LoginException;
import server.exceptions.LoginPermissionDeniedException;
import server.exceptions.NotAdminException;
import server.exceptions.ReflexiveActionException;
import server.exceptions.ServantException;
import client.BoardImage;
import client.BoardModification;
import client.ClientMessenger;
import client.SimpleGUI;
import client.WhiteboardGUI;

public class HeavyCentralWB implements CentralWhiteboard
{
    enum MessageType
    {
        boardUpdate,
        setImage,
        clientLogout,
        clientLogin,
        newAdmin,
        kick
    }
    
    private class ModClientPair
    {
        public String client;
        public BoardModification mod;
        public ModClientPair(String client, BoardModification mod)
        {
            this.client = client;
            this.mod = mod;
        }
    }
    
    
    private WhiteboardGUI serverGUI;
    private String currentAdminName;
    private String whiteboardName;
    private SimpleRemoteLogin loginServer;
    
    //object providinig lock for code that asks admin's permission for
    //something (to ensure admin is only being asked one such thing at
    //a time... we use a separate lock to other synchronized methods to
    //prevent server from hanging while awaiting admin response to a login
    //request, etc)
    private Object adminAskLock;
    
    //implement using linked-list rather than a set for faster processing
    //of attemptUpdate call (fast iterating over clients)
    private LinkedList<String> currentClients;
    private HashMap<String,ClientMessenger> messengerDict;
    private HashMap<String,ClientServant> servantDict;
    
    //this is flagged to true once last client leaves; allows us
    //to handle situation where new client tries to connect
    //after board is killed but before RemoteLogin can be cleaned
    private boolean boardDead;
    
    //ServerClock object for this whiteboard
    private ActivityClock clock;
    
    /*
     * Constructor for new CentralWhiteboard object given name
     * of initial administrator, starting BoardImage object, and
     * the name of the whiteboard instance itself
     */
    public HeavyCentralWB(String whiteboardName,
            SimpleRemoteLogin loginServer, String adminName,
            BoardImage initialImage)
    {
        this.whiteboardName = whiteboardName;
        this.currentAdminName = adminName;
        this.serverGUI = new SimpleGUI(initialImage);
        this.loginServer = loginServer;
        currentClients = new LinkedList<String>();
        messengerDict = new HashMap<String,ClientMessenger>();
        servantDict = new HashMap<String,ClientServant>();
        boardDead = false;
        
        adminAskLock = new Object();
        //set up server clock
        clock = new ActivityClock(this);
        //start the clock thread
        Thread clockThread = new Thread(clock);
        clockThread.start();
    }

    /*
     * In this simple implementation we never reject an update, so always
     * process update, alert other clients, and return true
     */
    public synchronized boolean attemptUpdate(String clientname,
            BoardModification modification)
    {
        serverGUI.updateBoard(modification, clientname);
        
        //now communicate update to all other clients
        this.messageAllClients(MessageType.boardUpdate,
                new ModClientPair(clientname, modification));
        
        return true;
    }

    /*
     * add entries to dictionaries of ClientServant and ClientMessenger objects
     */
    public void registerClientMessenger(String clientname,
                    ClientMessenger messenger, ClientServant servant)
            throws LoginException
    {
        //ensure only one login-attempt can be occurring at a time
        synchronized (adminAskLock)
        {
            //protected initial checks
            synchronized(this)
            {
                //first ensure that this whiteboard instance is still active
                if (boardDead)
                    throw new BoardNotFoundException(whiteboardName);
                //check that client with same name is not already logged in
                if (currentClients.contains(clientname))
                    throw new ClientDuplicateException(clientname, whiteboardName);
            }
            //if the new client is not the admin we need to seek permission from
            //admin to connect, and if successful relay to them the current board
            //image
            //loop until we gain permission, or login fails and exception is thrown
            while (!clientname.equals(currentAdminName))
            {
                try
                {
                    //protected dictionary access
                    ClientMessenger adminMessenger;
                    synchronized(this)
                    {
                        adminMessenger = messengerDict.get(currentAdminName);
                    }
                    boolean permissionGranted = adminMessenger
                            .relayAskLoginPermission(clientname);
                    //if permission is granted relay the current BoardImage and
                    //break out of loop (success)
                    if (permissionGranted)
                        break;
                    //if permission not granted throw an appropriate LoginException
                    else
                    {
                        throw new LoginPermissionDeniedException
                            (currentAdminName, whiteboardName);
                    }
    
                }
                catch (RemoteException e)
                {
                    //protected admin kick
                    synchronized(this)
                    {
                        //If remote exception occurred means current admin is gone,
                        //so need to set a new one and loop
                        
                        //kickClient method will take care of setting the new admin
                        this.kickClient(currentAdminName);
                        
                        //check that board is still alive (i.e. we have not kicked
                        //last remaining client); assuming this has not occurred
                        //we loop again
                        if (boardDead)
                            throw new BoardNotFoundException(whiteboardName);
                    }
                }
            }
            //assuming these checks succeed we allow client to connect
            
            //rest of method synchronized with central whiteboard
            synchronized(this)
            {
                //now we need to obtain a current board image
                BoardImage startingImage =
                        serverGUI.getBoardImage();
                
                //try to relay the BoardImage... if this fails just abort
                try
                {
                    messenger.relaySetImage(startingImage);
                }
                catch (RemoteException e1)
                {
                    throw new LoginException("Error: Remote Exception "
                            + "thrown when trying to relay starting "
                            + "board image");
                }
                
                
                //alert other clients that a new user has connected
                if (!currentClients.isEmpty())
                    this.messageAllClients(MessageType.clientLogin, clientname);
                    
                //update client list and dictionaries
                currentClients.add(clientname);
                messengerDict.put(clientname, messenger);
                servantDict.put(clientname, servant);
                
                //add new client to clock object
                clock.addClient(clientname);
            }
        }
    }

    /*
     * Let's server know that client is still active
     * (useful if we are kicking clients after a period of inactivity)
     */
    public synchronized void poke(String clientname)
    {
        clock.resetTime(clientname);
    }

    /*
     * client voluntarily disconnects from server
     */
    public synchronized void logout(String clientname)
    {
        //delegate to kickClient method (to avoid code duplication)
        this.kickClient(clientname);
    }
    
    
    /*
     * checks for administrator privileges, and that target client
     * exists, then delegates call to kickClient
     */
    public synchronized void kickPeer
            (String callerName, String clientToBeKicked)
            throws ServantException
    {
        //do sanity checks first
        this.checkAdminPrivelages(callerName, clientToBeKicked, "kick");
        
        //delegate to kickClient method
        this.kickClient(clientToBeKicked);
    }

    /*
     * checks for administrator privileges, and that target client
     * exists, then delegates call to setNewAdmin
     */
    public synchronized void promotePeer
            (String callerName, String clientToBePromoted)
            throws ServantException
    {
        //do sanity checks first
        this.checkAdminPrivelages(callerName, clientToBePromoted, "promote");
        
        //delegate to setNewAdmin method
        this.setNewAdmin(clientToBePromoted);
    }
    
    /*
     * simply return the currentClients object
     */
    public synchronized LinkedList<String> getClientList()
    {
        return currentClients;
    }

    /*
     * simply return the currentAdminName string
     */
    public synchronized String getAdminName()
    {
        return currentAdminName;
    }
    
    /*
     * Attempt to obtain a current BoardImage object from an active peer
     * if this fails (because there are no active peers) instead
     * return the most recent local copy (may be out of date)
     */
    public synchronized BoardImage getBoardImage(String callerName)
    {
        return serverGUI.getBoardImage();
        //return this.getBoardImageInternal(callerName);
    }
    
    /*
     * checks administrator privileges, and if successful sets the
     * new BoardImage (which is communicated to all clients)
     */
    public synchronized void setBoardImage
            (String callerName, BoardImage newImage)
            throws ServantException
    {
        //checks we have administrator privileges
        if (!currentAdminName.equals(callerName))
            throw new NotAdminException(currentAdminName);
        
        //communicate the new board image to all clients
        this.messageAllClients(MessageType.setImage, newImage);
        
        //saves this new version locally
        serverGUI.setBoardImage(newImage);
    }
    
    
    /*
     * this is called when we wish to disconnect a client from the whiteboard
     * server
     */
    public synchronized void kickClient(String clientname)
    {
        //check that client has not been kicked already
        //during clock procedure (if already kicked do nothing)
        if (!currentClients.contains(clientname))
            return;
        
        //attempt to alert client that they have been logged off
        try
        {
            this.sendMessage(clientname, MessageType.kick, null);
        }
        catch (RemoteException e)
        {
            //Do nothing if kick alert fails (as we are already commited
            //to kicking them and alerting other clients in any case)
        }
        
        //remove the client from client-list and dictionaries
        currentClients.remove(clientname);
        messengerDict.remove(clientname);
        servantDict.remove(clientname);
        
        //remove client from clock object
        clock.deleteClient(clientname);
        
        //now delegate to messageAllClients to message this to peers
        // (and handle any further deletions correctly)
        this.messageAllClients(MessageType.clientLogout, clientname);
    }
    
    /*
     * checks if board is dead
     */
    public synchronized boolean isBoardDead()
    {
        return boardDead;
    }
    
    /*
     * sends inactive warning to client; if they do not respond within
     * 'secondsToKick' seconds they will be kicked
     */
    public synchronized void warnInactiveClient
            (String clientname, int secondsToKick)
    {
        try
        {
            //check that client has not been removed from dict
            //during clock procedure
            if (currentClients.contains(clientname))
            {
                ClientMessenger m = messengerDict.get(clientname);
                m.relayWarnInactiveLogout(secondsToKick);
            }
                
        }
        catch (RemoteException e)
        {
            //client is gone, so kick
            this.kickClient(clientname);
        }
    }
    
    /*
     * sets the the new administrator to be client "newAdminName",
     * and alerts all other clients of the change
     */
    private void setNewAdmin(String newAdminName)
    {
        currentAdminName = newAdminName;
        //inform all clients of this change
        this.messageAllClients(MessageType.newAdmin, newAdminName);
    }

    /*
     * abstraction of pattern for sanity-checking against possible
     * exceptions for performing an administrator action on another
     * client
     */
    private void checkAdminPrivelages
            (String callerName, String targetName, String actionName)
            throws ServantException
    {
        //check that the caller is admin, the target exists, and that
        //action is not reflexive
        if (!currentAdminName.equals(callerName))
            throw new NotAdminException(currentAdminName);
        else if (!currentClients.contains(targetName))
            throw new ClientDoesNotExistException
                (targetName, whiteboardName);
        else if (callerName.equals(targetName))
            throw new ReflexiveActionException(actionName);
    }

    /*
     * General pattern for sending some message to all clients, with
     * correct handling under any circumstances of client(s) disappearing
     * mid-method
     * 
     * In addition handles setting a new admin if the current admin is no
     * longer logged in at the end of the method call
     */
    private void messageAllClients(MessageType mt, Object contents)
    {
        //initialize linked list to store any clients who are delted
        LinkedList<String> deletedClientList = new LinkedList<String>();
        for (String client : currentClients)
        {
            try
            {
                this.sendMessage(client,mt,contents);
            }
            catch (RemoteException e)
            {
                //We assume if RemoteException occurs that client
                //we tried to message has disappeared, so add to
                //deleted list
                deletedClientList.add(client);
            }
        }
        
        //copy the delete list into a second temporary list
        LinkedList<String> tempDeleteList = new LinkedList<String>();
        for (String client : deletedClientList)
            tempDeleteList.add(client);
        
        //iterate while the temp delete list is non-empty
        while (!tempDeleteList.isEmpty())
        {
            String nextClient = tempDeleteList.pop();
            //send "client logged out" message to all remaninging clinets
            for (String client : currentClients)
            {
                //skip over if client has already been deleted
                if (deletedClientList.contains(client))
                    continue;
                
                try
                {
                    this.sendMessage(client, 
                            MessageType.clientLogout, nextClient);
                }
                catch (RemoteException e)
                {
                    //as above assume client has disappeared if RemoteException
                    //occurs
                    deletedClientList.add(client);
                    tempDeleteList.add(client);
                }
            }
        }
        
        //now remove any deleted clients from the current client list,
        //and from the dictionaries
        for (String deletedClient : deletedClientList)
        {
            currentClients.remove(deletedClient);
            messengerDict.remove(deletedClient);
            servantDict.remove(deletedClient);
        }
        
        //if list of current clients now empty, flag for deletion
        if (currentClients.isEmpty())
        {
            boardDead = true;
            loginServer.deleteBoardInstance(whiteboardName);
            return;
        }
        
        //otherwise, if admin needs replacing, do so
        if (!currentClients.contains(currentAdminName))
        {
            //arbitrarily pick first client in list
            String newAdminName = currentClients.getFirst();
            this.setNewAdmin(newAdminName);
        }
        
        
    }

    /*
     * general method for sending a message of type mt to the client with name
     * given by "client", with message contents (whose type must be cast-able
     * to the type appropriate for the message type) given by "contents"
     */
    private void sendMessage(String client, MessageType mt, Object contents)
            throws RemoteException
    {
        switch(mt)
        {
        case boardUpdate:
            ModClientPair pair = (ModClientPair)contents;
            messengerDict.get(client).relayBoardUpdate
                    (pair.mod, pair.client);
            break;
        case clientLogin:
            messengerDict.get(client).relayAlertClientLogin((String)contents);
            break;
        case clientLogout:
            messengerDict.get(client).relayAlertClientLogout((String)contents);
            break;
        case newAdmin:
            messengerDict.get(client).relaySetNewAdmin((String)contents);
            break;
        case setImage:
            messengerDict.get(client).relaySetImage((BoardImage)contents);
            break;
        case kick:
            messengerDict.get(client).relayAlertKick();
        default:
            break;
        
        }
    }

}
