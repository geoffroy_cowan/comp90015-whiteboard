package server;

import java.rmi.AccessException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.NoSuchAlgorithmException;

import server.exceptions.UsageException;
import client.ClientLogin;
import client.SimpleClientLogin;

public class SimpleBootServer
{
    public static void main(String[] args)
    {
        String loginServerName = ClientLogin.loginServerName;
        
        //first command line argument should be port number of local
        //RMI registry
        int port;
        
        //if (System.getSecurityManager() == null) {
        //    System.setSecurityManager(new RMISecurityManager());
        //}
        
        try
        {
            //expect 2 command-line argument; port and hostname
            if (args.length != 2)
                throw new UsageException();
            
            try
            {
                port = Integer.valueOf(args[0]);
            }
            catch (NumberFormatException e)
            {
                throw new UsageException();
            }
            String hostname = args[1];
            System.setProperty("java.rmi.server.hostname", hostname);
            Registry reg = LocateRegistry.getRegistry(hostname, port);
            
            //create RemoteLogin object and rebind to registry
            RemoteLogin loginObject = new SimpleRemoteLogin();
            reg.rebind(loginServerName, loginObject);
            System.out.println("Server is running");
        }
        catch (AccessException e)
        {
            System.err.println("Error: permission for binding to"
                    + "registry denied");
        }
        catch (RemoteException e)
        {
            System.err.println("Error: RemoteException occured.");
            System.err.println("Please ensure that supplied port number"
                    + "is valid, and rmiregistry has been started on"
                    + "localhost with that port number.");
            System.err.println("Detailed exception message follows:");
            System.err.print(e.getMessage() + "\n");
        }
        catch (UsageException e)
        {
            System.err.print(e.getMessage());
        }
        catch(NoSuchAlgorithmException e)
        {
            System.err.print("Error: cipher algorithm "
                    + SimpleClientLogin.CIPHER_ALGORITHM + " is not available");
        }

    }

}
