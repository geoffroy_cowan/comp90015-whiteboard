package server;

import java.util.HashMap;
import java.util.LinkedList;

/*
 * object that acts as a rough clock for a central whiteboard, enforcing
 * timeouts
 * 
 * ticks approximately once per minute, and kicks inactive clients
 * after specified amount of time
 */
public class ActivityClock implements Runnable
{
    private CentralWhiteboard whiteboard;
    private HashMap<String,Integer> clientInactiveTimes;
    private LinkedList<String> clientList;
    private int listPointer;    //integer pointing to current element
                                //of list we are examining
    
    //time for which thread sleeps between clock ticks (in milliseconds)
    private final int clockTickInterval = 1000 * 60;
    //inactive time at which client is warned (in milliseconds)
    private final int warnTime = 1000 * 60 * 55;
    //inactive time at which client is kicked (in milliseconds)
    private final int kickTime = 1000 * 60 * 60;
    
    
    public ActivityClock(CentralWhiteboard whiteboard)
    {
        this.whiteboard = whiteboard;
        clientInactiveTimes = new HashMap<String,Integer>();
        clientList = new LinkedList<String>();
        listPointer = 0;
    }

    @Override
    public void run()
    {
        //continuously loop forever...
        while (true)
        {
            //sleep for one clock tick
            try
            {
                Thread.sleep(clockTickInterval);
            }
            catch (InterruptedException e)
            {
                //we never interrupt this thread, so should never occur
                System.err.println("Error: clock thread interrupted");
                return;
            }
            
            //if board is dead, terminate thread
            if (whiteboard.isBoardDead()) return;
            
            //otherwise iterate over all clients
            while (true)
            {
                //get next client, and increment their inactive time 
                // (do so synchronously)
                String nextClient;
                int inactiveTime;
                synchronized(this)
                {
                    int numClients = clientList.size();
                    if (listPointer >= numClients)
                    {
                        listPointer = 0;
                        break;
                    }
                    else
                    {
                        nextClient = clientList.get(listPointer);
                        inactiveTime = clientInactiveTimes.get(nextClient);
                        inactiveTime += clockTickInterval;
                        clientInactiveTimes.put(nextClient, inactiveTime);
                        listPointer += 1;
                    }
                }
                
                //check if client should be kicked
                // (asynchronous to avoid deadlock)
                if (inactiveTime >= this.kickTime)
                {
                    whiteboard.kickClient(nextClient);
                }
                //check if client should be warned
                else if (this.checkWarnTime(inactiveTime))
                {
                    //get time to kick, and convert from milliseconds to seconds
                    int secondsToKick = (kickTime - inactiveTime)/1000;
                    whiteboard.warnInactiveClient(nextClient, secondsToKick);
                }
            }
        }
    }
    
    /*
     * resets inactive time for given client
     */
    public synchronized void resetTime(String client)
    {
        clientInactiveTimes.put(client, 0);
    }
    
    /*
     * adds new client to list/dictionary
     */
    public synchronized void addClient(String client)
    {
        clientList.add(client);
        clientInactiveTimes.put(client, 0);
    }
    
    /*
     * deletes given client from the list/dictionary
     */
    public synchronized void deleteClient(String client)
    {
        //check if we are removing an element of list before list pointer;
        //if so we need to adjust pointer
        int listIndex = clientList.indexOf(client);
        if (listIndex < listPointer)
            listPointer -= 1;
        clientList.remove(client);
        clientInactiveTimes.remove(client);
    }
    
    /*
     * checks if at current inactive time we should send a warning to client
     * (do so first tick after reaching at least warnTime)
     */
    private boolean checkWarnTime(int time)
    {
        //if insufficient time past
        if (time < warnTime)
            return false;
        //if too much time past
        else if (time >= (warnTime + clockTickInterval))
            return false;
        else
            return true;
    }

}
