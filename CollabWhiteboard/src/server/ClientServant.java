package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.LinkedList;

import server.exceptions.ServantException;
import client.BoardImage;
import client.BoardModification;

/*
 * Interface for a Remote servant class that lives on server, and is exported
 * to client by reference (the implementation should extend UnicastRemoteObject,
 * and is obtained by client via login)
 * 
 * Allows client to communicate to the server
 */
public interface ClientServant extends Remote
{
    /*
     * method that should be called in order to modify the distributed
     * white-board
     */
    public boolean updateBoard(BoardModification modification)
            throws RemoteException, ServantException;


    /*
     * method used to let server know client is non inactive; resets
     * Inactivity timer without actually making any board modifications,
     * etc (can be used to avoid auto-disconnect)
     */
    public void pokeServer() throws RemoteException;


    /*
     * used to logout from server; user will be removed from server's
     * list of logged in clients (and thus attempts to modify board will
     * fail after this call)
     */
    public void logout() throws RemoteException;
    
    /*
     * method that can be called to kick another client from the server
     * (requires administrator privileges)
     */
    public void kickClient(String clientToKick)
            throws RemoteException, ServantException;
    
    /*
     * promotes another user to become the new administrator (and thus
     * also revokes the caller's administrator privileges)
     * (requires administrator privileges)
     */
    public void promoteClient(String clientToPromote)
            throws RemoteException, ServantException;
    
    /*
     * sets the board image to "newimage" (should not be used as main
     * update method)
     * (requires administrator privileges)
     */
    public void setBoardImage(BoardImage newimage)
            throws RemoteException, ServantException;

    /*
     * obtains a linked-list of the current clients connected to the
     * whiteboard instance
     */
    public LinkedList<String> getClientList()
            throws RemoteException;
    
    /*
     * obtains the name of the current administrator of the whiteboard
     * instance
     */
    public String getAdminName()
            throws RemoteException;
    
    /*
     * obtains the current BoardImage from the server (may or may not
     * actually prove useful, but just included in case there is some need
     * for this)
     * 
     * Note: it is recommended client does NOT use this as main source
     * of receiving updates from other clients
     * (and instead processes update calls made to ClientReceiver)
     */
    public BoardImage getBoardImage()
            throws RemoteException;
}
