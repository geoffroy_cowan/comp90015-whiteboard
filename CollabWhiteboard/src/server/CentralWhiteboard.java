package server;

import java.util.LinkedList;

import server.exceptions.LoginException;
import server.exceptions.ServantException;
import client.BoardImage;
import client.BoardModification;
import client.ClientMessenger;


/*
 * interface for the central white-board class on server which
 * receives update calls from remote Updater objects, and relays
 * them to Receiver objects of all other clients
 */
public interface CentralWhiteboard
{
    /* 
     * method that is called by a ClientServant in order to make an update
     * to the board given a BoardModification object
     * 
     * returns a boolean flag indicating success (if, for some reason,
     * update is considered illegal will return false)
     */

    public boolean attemptUpdate
            (String clientname, BoardModification modification)
            throws ServantException;

    /*
     * saves ClientMessenger object for the client with user-name
     * "clientname"
     * 
     * allows CentralWhiteboard class to communicate with client
     * 
     * in addition save reference to client's servant, which may prove
     * useful, and at worst should prevent garbage collection
     */
    public void registerClientMessenger(String clientname,
                ClientMessenger messenger, ClientServant servant)
            throws LoginException;

    /*
     * indicates that client given by "clientname" is still active; resets
     * Inactive timer for that client
     */
    public void poke(String clientname);

    /*
     * indicates that client "clientname" is logging off from server; clean up
     * references and alert other clients appropriately
     */
    public void logout(String clientname);
    
    /*
     * attempts to kick the client "clientToBeKicked"
     * requires the caller to be the current administrator
     */
    public void kickPeer(String callerName, String clientToBeKicked)
            throws ServantException;
    
    /*
     * attempts to promote the client "clientToBePromoted" to become
     * the new admin
     * requires the caller to be the current administrator
     */
    public void promotePeer(String callerName, String clientToBePromoted)
            throws ServantException;
    
    /*
     * obtains the list of all clients currently connected to the server
     */
    public LinkedList<String> getClientList();
    
    /*
     * obtains the name of the current administrator of server
     */
    public String getAdminName();
    
    /*
     * obtains the current board image
     */
    public BoardImage getBoardImage(String callerName);

    /*
     * sets the board image on server (requires administrator privileges)
     */
    public void setBoardImage(String callerName, BoardImage newimage)
            throws ServantException;
    
    /*
     * kicks a client from board
     */
    public void kickClient(String clientname);
    
    /*
     * checks if whiteboard is dead
     */
    public boolean isBoardDead();

    /*
     * sends inactive warning to client; if they do not respond within
     * 'secondsToKick' seconds they will be kicked
     */
    public void warnInactiveClient(String clientname, int secondsToKick);
}
