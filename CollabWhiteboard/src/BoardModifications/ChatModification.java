package BoardModifications;

import client.BoardModification;

public class ChatModification implements BoardModification{

    private static final long serialVersionUID = -5454332518058175770L;
    
    public String author;
    public String message;
    public String timestamp;
    
    public ChatModification(String author, String timestamp, String message){
        this.author= author;
        this.timestamp = timestamp;
        this.message = message;
    }

}
