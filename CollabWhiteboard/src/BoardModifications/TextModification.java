package BoardModifications;

import java.awt.Color;
import java.awt.Point;
//import java.awt.Stroke;

import client.BoardModification;

public class TextModification implements BoardModification{
    
    //public Stroke stroke;
    public Color color;
    public Point pointA;
    public Point pointB;
    public String text;
    
    public TextModification(String text, Color color, Point pointA, Point pointB){
        //this.stroke = stroke;
        this.text = text;
        this.color = color;
        this.pointA = pointA;
        this.pointB = pointB;
    }

}
