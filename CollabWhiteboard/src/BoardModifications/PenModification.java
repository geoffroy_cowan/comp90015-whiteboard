package BoardModifications;

import java.awt.Color;
import java.awt.Point;
import java.awt.Stroke;

import client.BoardModification;

public class PenModification implements BoardModification{

    private static final long serialVersionUID = -5454332518058175770L;
    
    public int strokeSize;
    public Color color;
    public Point pointA;
    public Point pointB;
    
    public PenModification(int strokeSize, Color color, Point pointA, Point pointB){
        this.strokeSize = strokeSize;
        this.color = color;
        this.pointA = pointA;
        this.pointB = pointB;
    }

}
