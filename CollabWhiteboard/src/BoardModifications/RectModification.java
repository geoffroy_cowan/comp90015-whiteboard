package BoardModifications;

import java.awt.Color;
import java.awt.Point;
import java.awt.Stroke;

import client.BoardModification;

public class RectModification implements BoardModification{
    
    public int strokeSize;
    public Color color;
    public Point pointA;
    public Point pointB;
    public boolean fill;
    
    public RectModification(int strokeSize, Color color, Point pointA, Point pointB, boolean fill){
        this.strokeSize = strokeSize;
        this.color = color;
        this.pointA = pointA;
        this.pointB = pointB;
        this.fill = fill;
    }

}
