package client;

import java.io.Serializable;
import java.awt.image.BufferedImage;

/**
 * a serializable interface used to represent the image of an entire
 * whiteboard.
 * 
 * Sometimes objects of this class will need to be transfered over network
 * (e.g. when new client connects they will first need to receive the entire
 * starting board state as of when they logged in), hence why this class
 * extends Serializable.
 * 
 * Also may be useful in allowing board images to be saved server-side, and
 * recovered at a later time.
 */
public interface BoardImage extends Serializable
{
    /**
     *  Recreate the BufferedImage that the BoardImage was constructed
     *  from.
     */
    public BufferedImage getImageFromPixels ();
}

// vim: ts=4 sw=4 et
