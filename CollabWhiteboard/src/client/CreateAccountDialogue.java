package client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import server.exceptions.AccountDetailsTooLongException;
import server.exceptions.CreateAccountException;

public class CreateAccountDialogue implements ActionListener
{

    private static final String CREATE_COMMAND = "create";
    private static final String CANCEL_COMMAND = "cancel";

    private ClientGUI parent;

    private JTextField username;
    private JPasswordField password;
    private JButton cancelButton, createButton;
    private JFrame dialogue;

    private ClientLogin loginObject;

    /**
     *  Initialise the GUI components that will be later added to panels.
     */
    public CreateAccountDialogue(ClientGUI parent)
    {
        username = new JTextField ();
        password = new JPasswordField ();

        dialogue = new JFrame ("Choose a username and password...");
        dialogue.setLayout (new GridLayout (0, 1));

        this.parent = parent;
        this.loginObject = parent.getClientLogin();
    }

    /**
     *  starts wizard for creating account; analogous to method in
     *  ServerSelectionDialogue
     */
    public static void startWizard(ClientGUI parent)
    {
        CreateAccountDialogue wizard = 
            new CreateAccountDialogue(parent);
        wizard.displayCreateAccountWindow ();
    }

    /**
     *  Display the dialogue frame
     */
    private void displayCreateAccountWindow ()
    {
        //check that server is initialized (this is entry point to
        //event based dialogue)
        if (loginObject == null)
        {
            this.displayErrorMessage("Need to setup connection details first!\n"
                    + "(via Network -> Setup Connection...)");
            dialogue.dispose();
            return;
        }
        
        JPanel panel = new JPanel (new GridLayout (0, 1));

        panel.add (new JLabel ("Select username and password for new account:"));
        panel.add (new JLabel ("Username"));
        panel.add (username);
        panel.add (new JLabel ("Password"));
        panel.add (password);
        panel.add (buildButtons ());

        dialogue.setContentPane (panel);
        dialogue.pack ();
        dialogue.setVisible (true);
    }
    
    /**
     * builds the "create" and "cancel" buttons to go at the bottem of
     * the dialogue frame
     */
    private JPanel buildButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));
    
        cancelButton = new JButton ("Cancel");
        cancelButton.setActionCommand (CANCEL_COMMAND);
        cancelButton.addActionListener (this);
    
        createButton = new JButton ("Create");
        createButton.setActionCommand (CREATE_COMMAND);
        createButton.addActionListener (this);
    
        buttonsPanel.add (cancelButton);
        buttonsPanel.add (createButton);
    
        return buttonsPanel;
    }
    
    /**
     *  This callback is invoked by the swing framework when the user
     *  clicks either the next or cancel buttons. We will determine which
     *  was clicked, and handle it accordingly.
     *  
     *  Where the real action happens
     */
    public void actionPerformed (ActionEvent e)
    {
        String buttonName = e.getActionCommand();

        if (buttonName.equals (CREATE_COMMAND))
        {
            tryCreateAccount();
        }
        else if (buttonName.equals (CANCEL_COMMAND))
        {
            // abort the dialog.
            dialogue.dispose ();
        }
    }

    private void tryCreateAccount()
    {
        ClientLogin loginObject = parent.getClientLogin();
        if (loginObject == null)
        {
            this.displayErrorMessage("Need to setup connection details"
                    + "first! (via Network -> Setup Connection...)");
            return;
        }
        loginObject.registerCredentials(username.getText(),
                password.getPassword());
        try
        {
            loginObject.createAccount();
            parent.logSuccessfulAccountCreate(username.getText());
            dialogue.dispose();
        }
        catch (RemoteException e)
        {
            this.displayErrorMessage("Error in accessing remote server; ensure\n"
                    + "that you are accessing a valid server with correct port\n"
                    + "number, and that server is actually booted!");
            //e.printStackTrace();
        }
        catch (AccountDetailsTooLongException e)
        {
            this.displayErrorMessage("Account creation failed because account\n"
                    + "details provided were too long for public key\n"
                    + "encryption... Try using a shorter username and/or"
                    + "password!");
        }
        catch (CreateAccountException e)
        {
            this.displayErrorMessage("Failure in creating account!\nExplanation"
                    + " for error provided by server:\n"
                    + e.getMessage());
            //e.printStackTrace();
        }
    }

    private void displayErrorMessage (String message)
    {
        JOptionPane.showMessageDialog (dialogue, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }
}
