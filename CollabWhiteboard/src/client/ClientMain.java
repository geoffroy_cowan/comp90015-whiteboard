package client;

public class ClientMain implements ClientReceiver {

    private ClientGUI gui;
    
    public ClientMain(){
        gui = new ClientGUI(this, "CollabWhiteboard", "USER");
    }

    /**
     *  Entry point of the whiteboard client program.
     */
    public static void main(String [] args) {
        //if (System.getSecurityManager() == null) {
        //    System.setSecurityManager(new SecurityManager());
        //}
        if (args.length != 1)
        {
            System.err.println("*** USAGE ***");
            System.err.println("java client.ClientMain [[host name]]");
            System.err.println("\"host name\" must be a valid name for client"
                    + "\non server machine");
            return;
        }
        String hostname = args[0];
        System.setProperty("java.rmi.server.hostname", hostname);
    	ClientMain client = new ClientMain();
    }
    
    public ClientGUI getClientGUI(){
        return this.gui;
    }
    
    @Override
    public void provideBoardUpdate(BoardModification modification, String updatingClient) {
        gui.updateBoard(modification, updatingClient);
    }

    @Override
    public void warnInactiveLogout(int secsToLogout) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void alertClientLogin(String newClientName) {
       gui.addUser(newClientName);
    }

    @Override
    public void alertClientLogout(String oldClientName) {
        gui.removeUser(oldClientName);
        
    }

    @Override
    public void setBoardImage(BoardImage image) {
        gui.setBoardImage (image);
    }

    @Override
    public boolean askLoginPermission(String newClientName) {
        return gui.getPermission(newClientName);
    }

    @Override
    public void setNewAdmin(String newAdminName) {
          gui.newAdmin(newAdminName);
    }

    @Override
    public BoardImage getCurrentImage() {
        return gui.getCurrentBoardImage ();
    }

    @Override
    public void alertKick() {
        gui.kick();
    }
    
    public void setBoardTitle(String title){
    	gui.setTitle(title);
    }

}

// vim: ts=4 sw=4 et
