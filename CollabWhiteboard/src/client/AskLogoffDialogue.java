package client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class AskLogoffDialogue implements ActionListener
{

    private static final String YES_COMMAND = "yes";
    private static final String NO_COMMAND = "no";

    private ClientGUI parent;
    private boolean loggedOff;

    private JButton noButton, yesButton;
    private JFrame dialogue;

    /**
     *  Initialise the GUI components that will be later added to panels.
     */
    public AskLogoffDialogue(ClientGUI parent, boolean loggedOff)
    {

        dialogue = new JFrame ("Confirm logoff...");
        dialogue.setLayout (new GridLayout (0, 1));

        this.parent = parent;
        this.loggedOff = loggedOff;
    }

    /**
     *  starts wizard for creating account; analogous to method in
     *  ServerSelectionDialogue
     */
    public static void startWizard(ClientGUI parent, boolean loggedOff)
    {
        AskLogoffDialogue wizard = 
            new AskLogoffDialogue(parent, loggedOff);
        wizard.displayAskLogoffWindow ();
    }

    /**
     *  Display the dialogue frame
     */
    private void displayAskLogoffWindow ()
    {
        if (loggedOff)
        {
            displayErrorMessage("Error: you are not logged in to\n"
                    + "a whiteboard instance!");
            dialogue.dispose();
            return;
        }
        JPanel panel = new JPanel (new GridLayout (0, 1));

        panel.add (new JLabel ("Do you really want to log off?"));
        panel.add (buildButtons ());

        dialogue.setContentPane (panel);
        dialogue.pack ();
        dialogue.setVisible (true);
    }
    
    /**
     * builds the "confirm" and "cancel" buttons to go at the bottom of
     * the dialogue frame
     */
    private JPanel buildButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));
    
        yesButton = new JButton ("Yes");
        yesButton.setActionCommand (YES_COMMAND);
        yesButton.addActionListener (this);
    
        noButton = new JButton ("No");
        noButton.setActionCommand (NO_COMMAND);
        noButton.addActionListener (this);
    
        buttonsPanel.add (noButton);
        buttonsPanel.add (yesButton);
    
        return buttonsPanel;
    }
    
    /**
     *  This callback is invoked by the swing framework when the user
     *  clicks either the confirm or cancel buttons. We will determine which
     *  was clicked, and handle it accordingly.
     *  
     *  Where the real action happens
     */
    public void actionPerformed (ActionEvent e)
    {
        String buttonName = e.getActionCommand();

        if (buttonName.equals (YES_COMMAND))
        {
            parent.logOff();
            dialogue.dispose();
        }
        else if (buttonName.equals (NO_COMMAND))
        {
            // abort the dialog.
            dialogue.dispose ();
        }
    }

   

    private void displayErrorMessage (String message)
    {
        JOptionPane.showMessageDialog (dialogue, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }
}
