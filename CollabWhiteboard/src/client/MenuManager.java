package client;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


/**
 *  A class to set up a menu bar, and respond to the user selecting items
 *  from the menus.
 */
public class MenuManager implements ActionListener
{
    // names of menu items.
    private static final String [] fileMenuLabels = 
        {"New", "Open...", "Save...", "Quit"};
    private static final String [] editMenuLabels = {"Choose Colour..."};
    private static final String [] netMenuLabels = 
        {"Setup Connection...", "Create New Account...", "Connect To Whiteboard...",
                "Log Off"};
    private static final String [] adminMenuLabels =
        {"Promote Peer", "Kick Peer"};

    private JMenuBar menuBar;
    
    // the top level GUI object whose methods will be invoked when the user
    // selects a menu item.
    private MenuCallback callBack; 

    /**
     *  Constructor. Create a new JMenuBar, and add to it file and
     *  edit menus.
     *
     *  @param owner Reference to the object that is creating the menu.
     */
        public
    MenuManager (MenuCallback owner)
    {
        menuBar = buildMenuBar ();
        callBack = owner;
    }

    /**
     *  Instantiate a new menu manager, initialise the menus, and bind them
     *  to a JFrame.
     *
     *  @param frame The GUI window to attach the menu bar to.
     *  @param owner Object to invoke on menu selections.
     */
        public static void
    initMenuBar (JFrame frame, MenuCallback owner)
    {
        MenuManager mm = new MenuManager (owner);
        frame.setJMenuBar (mm.menuBar);
    }

    /**
     *  Build a menu bar, with two menus, for file and edit.
     */
        private JMenuBar
    buildMenuBar ()
    {
        JMenuBar menuBar = new JMenuBar ();

        // create the file and edit menus.
        menuBar.add (buildMenu ("File", fileMenuLabels));
        menuBar.add (buildMenu ("Edit", editMenuLabels));
        menuBar.add (buildMenu ("Network", netMenuLabels));
        menuBar.add (buildMenu ("Admin", adminMenuLabels));

        return menuBar;
    }

    /**
     *  Create a menu, given the menu title, and a list of labels of menu
     *  items.
     *
     *  @param label The menu title
     *  @param menuLabels The names of all the menu items
     */
        private JMenu
    buildMenu (String label, String [] menuLabels)
    {
        JMenu menu = new JMenu (label);
        JMenuItem menuItem;

        // step through each name in the list of menu item names, create
        // new menu item objects and add them to the menu. Also, make sure
        // that we are notified when a menu item is selected.
        for (String menuLabel : menuLabels)
        {
            menuItem = new JMenuItem (menuLabel);
            menuItem.addActionListener (this);
            menu.add (menuItem);
        }

        return menu;
    }

    /**
     *  This method is called whenever the user clicks on a menu item. In
     *  this method, we will determine what item was clicked, and do
     *  something appropriate.
     *
     *  @param e Event information. What was clicked.
     */
        public void
    actionPerformed (ActionEvent e)
    {
        String itemSelected = e.getActionCommand ();

        // change colour?
        if (itemSelected.equals (editMenuLabels [0]))
            callBack.changeColor ();

        // Save board as image?
        if (itemSelected.equals (fileMenuLabels [2]))
            callBack.saveBoard ();

        // load an initial whiteboard state from file?
        if (itemSelected.equals (fileMenuLabels [1]))
            callBack.loadFromFile ();
        
        // load an initial whiteboard state from file?
        if (itemSelected.equals (fileMenuLabels [0]))
            callBack.createNew ();

        // quit app?
        if (itemSelected.equals (fileMenuLabels [3]))
            callBack.shutdown ();
        
        // setup connection details?
        if (itemSelected.equals (netMenuLabels [0]))
            callBack.setupConnectionDetails ();
        
        // create new account?
        if (itemSelected.equals (netMenuLabels [1]))
            callBack.createNewAccount ();
        
        // open whiteboard selection dialogue?
        if (itemSelected.equals (netMenuLabels [2]))
            callBack.selectServer ();
        
        // logoff?
        if (itemSelected.equals (netMenuLabels [3]))
            callBack.askLogoff ();
        
        // promote a peer?
        if (itemSelected.equals (adminMenuLabels [0]))
            callBack.promotePeer ();
        
        // kick a peer?
        if (itemSelected.equals (adminMenuLabels [1]))
            callBack.kickPeer ();
        
    }
}

// vim: ts=4 sw=4 et
