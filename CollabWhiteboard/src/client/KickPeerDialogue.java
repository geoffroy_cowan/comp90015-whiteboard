package client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import server.ClientServant;

public class KickPeerDialogue implements ActionListener, ListSelectionListener
{

    private static final String KICK_COMMAND = "kick";
    private static final String CANCEL_COMMAND = "cancel";
    private static final String REFRESH_COMMAND = "refresh";

    private boolean canKick;

    private JButton kickButton, cancelButton, refreshButton;
    private JFrame dialogue;
    private JList peersList;
    
    private String myName;
    private ClientServant clientServant;
    private String selectedPeer = "";

    /**
     *  Initialise the GUI components that will be later added to panels.
     */
    public KickPeerDialogue(boolean canKick,
            ClientServant clientServant, String myName)
    {

        dialogue = new JFrame ("Select a peer to kick...");
        dialogue.setLayout (new GridLayout (0, 1));

        this.canKick = canKick;
        this.clientServant = clientServant;
        this.myName = myName;
    }

    /**
     *  starts wizard for creating account; analogous to method in
     *  ServerSelectionDialogue
     */
    public static void startWizard(boolean canKick,
            ClientServant clientServant, String myName)
    {
        KickPeerDialogue wizard = 
            new KickPeerDialogue(canKick, clientServant, myName);
        wizard.displayKickPeerWindow ();
    }

    /**
     *  Display the dialogue frame
     */
    private void displayKickPeerWindow ()
    {
        if (!canKick)
        {
            displayErrorMessage("Invalid Action:\n"
                    + "Must be the administrator of a\n"
                    + "whiteboard session!");
            dialogue.dispose();
            return;
        }
        JPanel panel = new JPanel (new GridLayout (0, 1));
        
        this.buildPeersList();

        panel.add (new JLabel ("Choose a peer to kick:"));
        panel.add(peersList);
        panel.add (buildButtons ());

        dialogue.setContentPane (panel);
        dialogue.pack ();
        dialogue.setVisible (true);
    }
    
    /**
     * builds the list of client peers that can be kicked
     */
    private void buildPeersList ()
    {
        DefaultListModel peerNames = new DefaultListModel ();

        try
        {
            for (String name : clientServant.getClientList())
            {
                if (!name.equals(myName))
                    peerNames.addElement (name);
            }
                
        }
        catch (Exception e)
        {
            System.err.println ("Could not get list of peers:");
            e.printStackTrace ();
        }

        peersList = new JList (peerNames);
        peersList.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
        peersList.addListSelectionListener (this);
    }

    
    /**
     * builds the "confirm" and "cancel" buttons to go at the bottom of
     * the dialogue frame
     */
    private JPanel buildButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));
    
        kickButton = new JButton ("Kick");
        kickButton.setActionCommand (KICK_COMMAND);
        kickButton.addActionListener (this);
    
        cancelButton = new JButton ("Cancel");
        cancelButton.setActionCommand (CANCEL_COMMAND);
        cancelButton.addActionListener (this);
        
        refreshButton = new JButton ("Refresh");
        refreshButton.setActionCommand (REFRESH_COMMAND);
        refreshButton.addActionListener (this);
    
        buttonsPanel.add (refreshButton);
        buttonsPanel.add (cancelButton);
        buttonsPanel.add (kickButton);
    
        return buttonsPanel;
    }
    
    /**
     *  This callback is invoked by the swing framework when the user
     *  clicks either the confirm or cancel buttons. We will determine which
     *  was clicked, and handle it accordingly.
     *  
     *  Where the real action happens
     */
    public void actionPerformed (ActionEvent e)
    {
        String buttonName = e.getActionCommand();

        if (buttonName.equals (KICK_COMMAND))
        {
            if (!selectedPeer.equals(""))
                try
                {
                    clientServant.kickClient(selectedPeer);
                    dialogue.dispose();
                }
                catch (Exception e1)
                {
                    this.displayErrorMessage("Unexpected error occurred\n"
                            + "while trying to kick peer!");
                }
        }
        else if (buttonName.equals (REFRESH_COMMAND))
        {
            // refresh list of peers
            this.selectedPeer = "";
            this.displayKickPeerWindow();
        }
        else if (buttonName.equals(CANCEL_COMMAND))
        {
            //quit out
            dialogue.dispose();
        }
    }
    
    /**
     *  Whenever the user selects an item in the list of peers,
     *  this method gets called. It will store the selected
     *  peer name
     */
    public void valueChanged (ListSelectionEvent e)
    {
        selectedPeer  = (String) peersList.getSelectedValue ();
    }

   

    private void displayErrorMessage (String message)
    {
        JOptionPane.showMessageDialog (dialogue, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }
}
