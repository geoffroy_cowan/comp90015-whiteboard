package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for messenger class, which will live on client machine
 * and accept remote calls, via RMI, from the server machine.
 *
 * The implementations of these methods should make use of the methods
 * defined by the ClientReceiver interface to informe the client of
 * the various events.
 * 
 * (do this from this object rather than directly from ClientServant so that
 * we do not have to serialize and store a copy of the ClientReceiver
 * on the server; instead we can just keep a reference to this on server;
 * makeing for lightweight, two-way communication).
 */
public interface ClientMessenger extends Remote
{
    
    /**
     * Another client has drawn on the whiteboard. This method should
     * hand the modification to the ClientReceiver by using the
     * provideBoardUpdate method.
     *
     * @param modification The modification that the other whiteboard
     *      client has made, and which we need to apply to our display.
     */
    public void relayBoardUpdate(BoardModification modification, String updatingClient)
            throws RemoteException;
    
    /**
     * We have been inactive for a while, and will be logged out
     * automatically soon if we don't do anything. This method should
     * invoke the warnInactiveLogout method in ClientReceiver.
     *
     * @param secsToLogout Time we have to cancel automatic logout.
     */
    public void relayWarnInactiveLogout(int secsToLogout)
            throws RemoteException;
    
    /**
     * Another client has joined our whiteboard session. That is to say,
     * the session administrator has already approved them to join.
     */
    public void relayAlertClientLogin(String newClientName)
            throws RemoteException;
    
    /**
     * Another client has left our whiteboard session.
     */
    public void relayAlertClientLogout(String oldClientName)
            throws RemoteException;
    
    /*
     * relays call to setBoardImage in ClientReceiver
     */
    public void relaySetImage(BoardImage image)
            throws RemoteException;
    
    /**
     * A client who wishes to join our whiteboard session is asking for
     * permission. This method will only be invoked if we are the session
     * admin.
     */
    public boolean relayAskLoginPermission(String newClientName)
            throws RemoteException;
    
    /**
     * The session admin left, and one of the remaining clients has been
     * promoted to admin. The server chooses a new admin at random, so the
     * new admin might very well be us, in which case newAdminName will
     * match our name.
     */
    public void relaySetNewAdmin(String newAdminName)
            throws RemoteException;
    
    /*
     * relays call to getCrrentImage in ClientReceiver
     */
    public BoardImage relayGetCurrentImage()
            throws RemoteException;

    /**
     * We have been evicted from the session by the admin.
     */
    public void relayAlertKick()
            throws RemoteException;
}

// vim: set ts=4 sw=4 et
