package client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SetupConnectionDetailsDialogue implements ActionListener
{

    private static final String CONFIRM_COMMAND = "confirm";
    private static final String CANCEL_COMMAND = "cancel";

    private ClientGUI parent;
    private ClientReceiver receiver;

    private JTextField serverName, portNumber;
    private JButton cancelButton, confirmButton;
    private JFrame dialogue;

    /**
     *  Initialise the GUI components that will be later added to panels.
     */
    public SetupConnectionDetailsDialogue(ClientGUI parent, ClientReceiver receiver)
    {
        serverName = new JTextField ();
        portNumber = new JTextField ();

        dialogue = new JFrame ("Selecting server name and port number...");
        dialogue.setLayout (new GridLayout (0, 1));

        this.parent = parent;
        this.receiver = receiver;
    }

    /**
     *  starts wizard for creating account; analogous to method in
     *  ServerSelectionDialogue
     */
    public static void startWizard(ClientGUI parent, ClientReceiver receiver)
    {
        SetupConnectionDetailsDialogue wizard = 
            new SetupConnectionDetailsDialogue(parent, receiver);
        wizard.displaySetupConnectionWindow ();
    }

    /**
     *  Display the dialogue frame
     */
    private void displaySetupConnectionWindow ()
    {
        JPanel panel = new JPanel (new GridLayout (0, 1));

        panel.add (new JLabel ("Enter name and port number of whiteboard server:"));
        panel.add (new JLabel ("Server Name (or IP address)"));
        panel.add (serverName);
        panel.add (new JLabel ("Port Number"));
        panel.add (portNumber);
        panel.add (buildButtons ());

        dialogue.setContentPane (panel);
        dialogue.pack ();
        dialogue.setVisible (true);
    }
    
    /**
     * builds the "confirm" and "cancel" buttons to go at the bottom of
     * the dialogue frame
     */
    private JPanel buildButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));
    
        cancelButton = new JButton ("Cancel");
        cancelButton.setActionCommand (CANCEL_COMMAND);
        cancelButton.addActionListener (this);
    
        confirmButton = new JButton ("Confirm");
        confirmButton.setActionCommand (CONFIRM_COMMAND);
        confirmButton.addActionListener (this);
    
        buttonsPanel.add (cancelButton);
        buttonsPanel.add (confirmButton);
    
        return buttonsPanel;
    }
    
    /**
     *  This callback is invoked by the swing framework when the user
     *  clicks either the confirm or cancel buttons. We will determine which
     *  was clicked, and handle it accordingly.
     *  
     *  Where the real action happens
     */
    public void actionPerformed (ActionEvent e)
    {
        String buttonName = e.getActionCommand();

        if (buttonName.equals (CONFIRM_COMMAND))
        {
            trySetupConnection();
        }
        else if (buttonName.equals (CANCEL_COMMAND))
        {
            // abort the dialog.
            dialogue.dispose ();
        }
    }

    private void trySetupConnection()
    {
        int portNumInt;
        try
        {
            portNumInt = Integer.parseInt(portNumber.getText());
        }
        catch (NumberFormatException e1)
        {
            this.displayErrorMessage("Supplied port number was of an invalid format!"
                    + " (must be numeric)");
            return;
            //e1.printStackTrace();
        }
        try
        {
            ClientLogin loginObject = new SimpleClientLogin(serverName.getText(), portNumInt);
            loginObject.registerReceiver(receiver);
            parent.setServerName(serverName.getText());
            parent.setClientLogin(loginObject);
            dialogue.dispose();
        }
        catch (Exception e)
        {
            this.displayErrorMessage("Error in connectiong to server with supplied\n"
                    + "details. Ensure that details are correct and that server\n"
                    + "has actually been booted!");
            e.printStackTrace();
        }
    }

    private void displayErrorMessage (String message)
    {
        JOptionPane.showMessageDialog (dialogue, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }
}
