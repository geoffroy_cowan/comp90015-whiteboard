package client;

import java.rmi.RemoteException;
import java.util.LinkedList;

import server.ClientServant;
import server.exceptions.CreateAccountException;
import server.exceptions.LoginException;


/**
 * <pre>
 * interface for class that provides framework for logging into a remote
 * whiteboard instance on some server, setting up a new whiteboard instance,
 * setting up a user account, etc.
 * 
 * constructor for a ClientLogin instance requires:
 *  1) name of server hosting the whiteboard instances
 *  2) port number they use for their RMI registry
 *  
 *  constructor should always take form:
 *  public CONSTRUCTOR_CLASS_NAME(String servername, int port)
 *  
 *  
 *  
 *  *** EXAMPLE USE ***
 *  
 *  the following example skeleton could serve as a basis for logging in
 *  to server named "servername" on port "port", using receiver
 *  object "receiver", connecting to existing whiteboard instance
 *  with name "whiteboardName" using username "username" and password
 *  "password"
 *  
 *  returns object "servant" which is a proxy for ClientServant instance
 *  to allow for communication with server
 *  
 *  (in order to setup and connect to a new whiteboard instance, instead of
 *  an existing one, replace call to "loginToExisting" with call to
 *  "loginToNew", and supply additional argument containing initial BoardImage)
 *  
 *  *** CODE SKELETON FOLLOWS ***
 *  
 *  {@code
 *  try
 *  {
 *      SimpleClientLogin loginServer = new SimpleClientLogin
 *              (servername, port);
 *      ClientServant servant = loginServer.loginToExisting(username,
 *              password, whiteboardName, receiver);
 *      ...
 *  }
 *  catch (LoginException e)
 *  {
 *      //means login failed for some reason (e.g. invalid credentials, tried
 *      //to log in to a non-existent whiteboard instance, etc)
 *      //String describing failure can be obtained via e.getMessage(),
 *      //which can be provided to client for feedback
 *      ...
 *  }
 *  catch (RemoteException e)
 *  {
 *      //means low level failure occurred with RMI (i.e. failure occurred
 *      //in retrieving and/or invoking methods of remote object
 *      ...
 *  }
 *  catch (NotBoundException e)
 *  {
 *      //means the RemoteLogin remote object was not bound correctly
 *      //to the RMI registry on the server machine
 *      ...
 *  }
 *  }
 *  </pre>
 */
public interface ClientLogin
{
    //the name under which the RemoteLogin object is stored
    //in RMI registry on server
    public final static String loginServerName = "Login";
    
    /**
     * saves the user's username/password in the ClientLogin object
     * if username/password had already been saved overwrites previous
     * values
     * 
     * @param username the client's username
     * @param password the client's password
     * 
     * @return void
     */
    public void registerCredentials(String username, char [] password);
    
    /**
     * saves the user's ClientReceiver object, as the object to be used
     * for reverse communication
     * if a receiver had previously been saved it is discarded and new
     * one is saved
     * 
     * @param receiver the object implementing ClientReceiver interface
     *      which will facilitate communication from server to client
     *      (implemented by the client)
     */
    public void registerReceiver(ClientReceiver receiver)
            throws RemoteException;
    
    /**
     * method that can be used to client in order to supply
     * login credentials, in order to create connection with server.
     * 
     * Returns a ClientServant object facilitating connection.
     *  
     * Will throw exceptions if something is wrong (e.g. can't find server,
     * the RemoteLogin object was not bound to correct name in RMI registry,
     * etc), which should be handled by GUI to give nice feedback to user.
     * 
     * Note: if username/password and ClientReceiver have not been registered
     * LoginException is thrown
     *
     * @param whiteBoardName The name of the session to log in to.
     *
     * @return An object providing the client with RMI methods exported by
     *      the server (implements ClientServant interface).
     */
    public ClientServant
            loginToExisting(String whiteBoardName)
            throws RemoteException, LoginException;
    
    /**
     * Create a new whiteboard session on the server, with this client as
     * the session administrator.
     * 
     * Note: if username/password and ClientReceiver have not been registered
     * LoginException is thrown
     *
     * @param whiteBoardName The name to associate with the newly created
     *      session.
     * @param startingImage The initial contents of the whiteboard. If the
     *      whiteboard is initially empty, this will simply be a white
     *      fill, for example.
     *
     * @return An object providing the client with RMI methods exported by
     *      the server.
     */
    public ClientServant
            loginToNew(String whiteBoardName,BoardImage startingImage)
            throws RemoteException, LoginException;
    
    /**
     * List all the whiteboard sessions available on the server.
     *
     * @return A linked list of whiteboard session names.
     */
    public LinkedList<String>
            listWhiteboardInstances()
            throws RemoteException;
    
    /**
     * Used for registering a new account on the server with the supplied
     * username and password.
     * 
     * throws CreateAccountException if account creation failed for any reason
     * 
     */
    public void createAccount()
            throws RemoteException, CreateAccountException;
    
}

// vim: set ts=4 sw=4 et
