package client;


/**
 *  Defines the callback methods which are invoked by the menu manager
 *  when the user selects a menu item.
 */
public interface MenuCallback
{
    /**
     *  Change the color of the pen.
     */
    public void changeColor ();

    /**
     *  Save the current whiteboard state to the local machine, as an
     *  image file.
     */
    public void saveBoard ();

    /**
     *  Load an image file from the local file system, and place it on the
     *  whiteboard.
     */
    public void loadFromFile ();

    /**
     *  Select a remote whiteboard server to connect to.
     */
    public void selectServer ();

    /**
     *  Exit the whiteboard client application.
     */
    public void shutdown ();

    /**
     * setup the hostname / port number of whiteboard server
     */
    public void setupConnectionDetails();

    /**
     * create a new user account on the whiteboard server
     */
    public void createNewAccount();

    /**
     * kick a peer (only usable as admin)
     */
    public void kickPeer();

    /**
     * logs user off of current instance (with confirmation check first)
     */
    public void askLogoff();

    /**
     * resets to blank canvas
     */
    public void createNew();

    /**
     * promote a peer (only usable as admin)
     */
    public void promotePeer();
}

// vim: ts=4 sw=4 et
