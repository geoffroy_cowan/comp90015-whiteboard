package client;


/*
 * an interface that should be implemented by some class on client side
 * 
 * needs to accept calls to various methods in order to facilitate
 * communication from servant to client
 */
public interface ClientReceiver
{
	/*
	 * method that will be called when a modification is made to the board
	 * by another connected client
	 * 
	 * the update was made by client "updatingClient"
	 * 
	 * should serve call by updating client's GUI appropriately
	 */
	public void provideBoardUpdate(BoardModification modification,
	        String updatingClient);
	
    /*
     * warns client that they will be automatically logged out after
     * "secsToLogout" seconds, unless they make some activity
     * 
     * (up to others how they want to service this; e.g. could create pop-up
     * warning user of this, and call pokeServer method in ClientServant
     * if user responds by clicking OK)
     */
    public void warnInactiveLogout(int secsToLogout);
    
    /*
     * alerts user that a new client has logged on to the whiteboard
     * (e.g. may be useful in maintaining GUI that lists current users
     * logged in)
     * new client has username newClientName
     */
    public void alertClientLogin(String newClientName);
    
    /*
     * similar to abode, but instead alerts that a client has logged out
     * (as above may be useful in maintaining list of current clients
     * on board)
     */
    public void alertClientLogout(String oldClientName);
    
    /*
     * Alerts client that the entire whiteboard image should be reset
     * as described by image
     */
    public void setBoardImage(BoardImage image);

    /*
     * Will only be called if client is the administrator for the current
     * whiteboard instance
     * 
     * called when we seek permission of whether client with name
     * newClientName should be allowed to login to the whiteboard
     * 
     * should return true if we allow them to login, or false if we
     * want to reject them
     * 
     * Note: for a simple implementation if we do not want to bother about
     * this feature could just implement by unconditionally returning true
     */
    public boolean askLoginPermission(String newClientName);

    /*
     * Alerts client that the admin has changed to the user with username
     * "newAdminName"
     * 
     * client needs to explicitly check whether this is their username
     * (in which case they are now admin), as we do not make a separate
     * method call to the new administrator
     */
    public void setNewAdmin(String newAdminName);

    /*
     * asks the client for a current BoardImage object (needed for our
     * thin-server design, as the server does not store an up to date
     * BoardImage)
     */
    public BoardImage getCurrentImage();

    /*
     * alerts client that they have been kicked from server
     */
    public void alertKick();
}
