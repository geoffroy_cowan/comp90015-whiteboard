package client;

public class Tools {
    public static final int PEN    = 0;
    public static final int RECT   = 1;
    public static final int CIRCLE = 2;
    public static final int TEXT   = 3;
    public static final int CHAT   = 4;
    public static final int ERASER = 5;
    public static final int LINE   = 6;
}
