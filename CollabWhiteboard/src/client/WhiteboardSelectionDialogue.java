package client;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import server.ClientServant;
import server.exceptions.AuthenticationException;
import server.exceptions.BoardNameExistsException;
import server.exceptions.ClientDuplicateException;
import server.exceptions.LoginException;
import server.exceptions.LoginPermissionDeniedException;



/**
 *  A class to display a dialog box for the user to login in to
 *  a whiteboard instance on server (server connection must already
 *  exist)
 */
public class WhiteboardSelectionDialogue implements ActionListener,
    ListSelectionListener
{
    private enum DialogueState 
    {
        SESSION, LOGIN
    }

    private static final String LOGIN_COMMAND = "login";
    private static final String CANCEL_COMMAND = "cancel";
    private static final String REFRESH_COMMAND = "refresh";
    private static final String NEXT_COMMAND = "next";

    private ClientGUI parent;

    private JTextField userName, newSessionName;
    private JPasswordField password;
    private JList sessionList;
    private JButton loginButton, cancelButton, refreshButton, nextButton;
    private JFrame dialogue;

    private DialogueState currentWindow;
    private ClientLogin loginObject;
    private String sessionName = "";
    private boolean createNewSession, finished = false;
    private String currentWhiteboardName;

    /**
     *  Initialise the GUI components that will be later added to panels.
     */
    public WhiteboardSelectionDialogue (ClientGUI parent, String currentWhiteboardName)
    {
        loginObject = parent.getClientLogin();
        userName = new JTextField ();
        newSessionName = new JTextField ();
        password = new JPasswordField ();

        dialogue = new JFrame ("Choose a server to connect to...");
        dialogue.setLayout (new GridLayout (0, 1));
        
        this.currentWhiteboardName = currentWhiteboardName;

        this.parent = parent;
    }

    /**
     *  Display the wizard that steps the user through connecting to a
     *  server, and choosing a whiteboard session to join. Because the
     *  GUI is event driven (ie. non-blocking), this method will return
     *  as soon as the selection window is open. The RMI handle to the
     *  whiteboard session will be passed back via a callback.
     */
    public static void startWizard (ClientGUI parent, String currentWhiteboardName)
    {
        WhiteboardSelectionDialogue wizard = 
            new WhiteboardSelectionDialogue (parent, currentWhiteboardName);
        wizard.displaySessionList ();
    }

    private void displaySessionList ()
    {
        //check that server is initialized (this is entry point to
        //event based dialogue)
        if (loginObject == null)
        {
            this.displayErrorMessage("Need to setup connection details first!\n"
                    + "(via Network -> Setup Connection...)");
            dialogue.dispose();
            return;
        }
        JPanel panel = new JPanel (new GridLayout (0, 1));

        buildSessionList ();

        panel.add (new JLabel ("Select from existing whiteboards:"));
        panel.add (sessionList);
        panel.add (new JLabel ("-- OR --"));
        panel.add (new JLabel ("Start a new whiteboard"));
        panel.add (newSessionName);
        panel.add (buildSessionListButtons ());

        dialogue.setContentPane (panel);
        dialogue.pack ();
        dialogue.setVisible (true);
        
        currentWindow = DialogueState.SESSION;
    }

    /**
     * builds the buttons displayed at the SessionList window
     * @return JPanel representing buttons
     */
    private JPanel  buildSessionListButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));

        nextButton = new JButton ("Next");
        nextButton.setActionCommand (NEXT_COMMAND);
        nextButton.addActionListener (this);

        cancelButton = new JButton ("Cancel");
        cancelButton.setActionCommand (CANCEL_COMMAND);
        cancelButton.addActionListener (this);
        
        refreshButton = new JButton ("Refresh");
        refreshButton.setActionCommand (REFRESH_COMMAND);
        refreshButton.addActionListener (this);

        buttonsPanel.add (refreshButton);
        buttonsPanel.add (cancelButton);
        buttonsPanel.add (nextButton);

        return buttonsPanel;
    } 
        
        
    public void buildSessionList ()
    {
        DefaultListModel sessionNames = new DefaultListModel ();

        try
        {
            for (String name : loginObject.listWhiteboardInstances ())
                sessionNames.addElement (name);
        }
        catch (Exception e)
        {
            System.err.println ("Could not get list of sessions:");
            e.printStackTrace ();
        }

        sessionList = new JList (sessionNames);
        sessionList.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
        sessionList.addListSelectionListener (this);
    }

    private boolean leaveSessionList ()
    {
        String newSession = newSessionName.getText ();
        boolean result = true;

        if (sessionName.equals ("") && !(newSession.equals ("")))
        {
            // the user wishes to create a new session.
            sessionName = newSession;
            createNewSession = true;
        }
        else if (sessionName.equals ("") != true)
        {
            // sign in to an existing session.
            createNewSession = false;
        }
        else
        {
            // user has not filled in either field.
            displayErrorMessage ("You need to tell me either the name\n" +
                    "of the whiteboard you wish to join,\nor the name " +
                    "of a whiteboard to be created.");
            result = false;
        }

        return result;
    }

    private void displayLoginScreen ()
    {
        JPanel panel = new JPanel (new GridLayout (0, 1));

        panel.add (new JLabel ("Authentication required:"));
        panel.add (new JLabel ("Username"));
        panel.add (userName);
        panel.add (new JLabel ("Password"));
        panel.add (password);
        panel.add (buildLoginButtons ());

        dialogue.setContentPane (panel);
    }
        
    /**
     * builds buttons for login screen
     * @return JPanel containing buttons
     */
    private JPanel buildLoginButtons ()
    {
        JPanel buttonsPanel = new JPanel (new GridLayout (0, 2));

        loginButton = new JButton ("Login");
        loginButton.setActionCommand (LOGIN_COMMAND);
        loginButton.addActionListener (this);

        cancelButton = new JButton ("Cancel");
        cancelButton.setActionCommand (CANCEL_COMMAND);
        cancelButton.addActionListener (this);

        buttonsPanel.add (cancelButton);
        buttonsPanel.add (loginButton);

        return buttonsPanel;
    }

    private boolean leaveLoginScreen ()
    {
        ClientServant servant;
        boolean result = false;

        if (sessionName.equals(currentWhiteboardName))
        {
            displayErrorMessage ("Invalid action: already logged\n"
                    + "in to this whiteboard!");
            return false;
        }

        try
        {
            loginObject.registerCredentials (userName.getText(), 
                    password.getPassword());
            servant = this.getClientServant();
            parent.setUserName (userName.getText());
            parent.setWhiteboardName(sessionName);
            parent.setClientServant(servant);
            result = true;
        }
        catch (AuthenticationException e)
        {
            //e.printStackTrace();
            displayErrorMessage ("Error: Authentication failed!\n"
                    + "Most likely causes are either password is\n"
                    + "spelled incorrectly, or user account has not\n"
                    + "been created");
        }
        catch (LoginPermissionDeniedException e)
        {
            displayErrorMessage ("Error: Login permission denied\n"
                    + "by administrator!");
        }
        catch (ClientDuplicateException e)
        {
            displayErrorMessage ("Error: user with name \"" + userName.getText()
                    + "\"\nis already logged in to whiteboard instance!\n"
                    + "Either you are already logged in,\n"
                    + "or your account is compromised.");
        }
        catch (BoardNameExistsException e)
        {
            displayErrorMessage ("Error: a whiteboard with this\n"
                    + "name already exists!");
        }
        catch (Exception e)
        {
            displayErrorMessage ("Error: Critical, unexpected\n"
                    + "error occurred!!");
            //e.printStackTrace();
        }
        /*
        catch (Exception e)
        {
            e.printStackTrace ();
            displayErrorMessage ("Error: Authentication failed!\n"
                    + "Most likely causes are either password is\n"
                    + "spelled incorrectly, or user account has not\n"
                    + "been created");
        }
        */

        return result;
    }

    private ClientServant getClientServant ()
            throws RemoteException, LoginException
    {
        ClientServant servant;

        if (createNewSession)
        {
            servant = 
                loginObject.loginToNew (sessionName, parent.getBoardImage ());
        }
        else
        {
            servant = loginObject.loginToExisting (sessionName);
        }

        return servant;
    }



    /**
     *  This callback is invoked by the swing framework when the user
     *  clicks either the next or cancel buttons. We will determine which
     *  was clicked, and handle it accordingly.
     */
    public void  actionPerformed (ActionEvent e)
    {
        String buttonName = e.getActionCommand ();

        if (buttonName.equals (NEXT_COMMAND) || buttonName.equals (LOGIN_COMMAND))
        {
            displayNextWindow ();
        }
        else if (buttonName.equals (REFRESH_COMMAND))
        {
            // refresh list of board instances (re display
            // session-list window)
            newSessionName.setText("");
            sessionName = "";
            displaySessionList();
        }
        else if (buttonName.equals (CANCEL_COMMAND))
        {
            // abort the dialog.
            dialogue.dispose ();
        }
    }

    private void displayNextWindow ()
    {
        if (confirmLeftCurrentWindow ())
        {
            enterNextWindow ();

            // don't repaint the login screen if the user has just logged
            // in successfully.
            if (finished)
                return;

            // update the dialog to display the new elements that we have
            // just added.
            dialogue.pack ();
            dialogue.setVisible (true);
        }
    }

    private boolean confirmLeftCurrentWindow ()
    {
        boolean result;

        if (currentWindow == DialogueState.SESSION)
        {
            result = leaveSessionList ();
        }
        else
        {
            result = leaveLoginScreen ();
        }

        return result;
    }

    private void enterNextWindow ()
    {
        if (currentWindow == DialogueState.SESSION)
        {
            displayLoginScreen ();
            currentWindow = DialogueState.LOGIN;
        }
        else
        {
            // once the user has logged in, the dialog has served its
            // purpose.
            dialogue.dispose ();
            finished = true;
        }
    }

    /**
     *  Whenever the user selects an item in the list of whiteboard
     *  sessions, this method gets called. It will store the selected
     *  session name in the appropriate member variable for when the user
     *  later clicks the "next" button.
     */
    public void valueChanged (ListSelectionEvent e)
    {
        sessionName = (String) sessionList.getSelectedValue ();
    }

    private void displayErrorMessage (String message)
    {
        JOptionPane.showMessageDialog (dialogue, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }
}

// vim: ts=4 sw=4 et
