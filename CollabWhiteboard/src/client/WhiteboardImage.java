package client;

import java.awt.image.BufferedImage;

public class WhiteboardImage implements BoardImage{
    private int imageWidth, imageHeight, imageType;
    private int [] pixels;
    
    public WhiteboardImage(BufferedImage image){
        pixels = getPixelArray (image);
        imageWidth = image.getWidth ();
        imageHeight = image.getHeight ();
        imageType = image.getType ();
    }

    private int [] getPixelArray (BufferedImage image) {
        int width = image.getWidth (), height = image.getHeight ();
        int [] temp = new int [width * height];
        return image.getRGB (0, 0, width, height, temp, 0, width);
    }

    public BufferedImage getImageFromPixels () {
        BufferedImage image = 
            new BufferedImage (imageWidth, imageHeight, imageType);
        image.setRGB (0, 0, imageWidth, imageHeight, pixels, 0, imageWidth);
        return image;
    }
}

// vim: ts=4 sw=4 et
