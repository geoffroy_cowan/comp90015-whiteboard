package client;

//TODO abstract drawing of shapes to external classes - pass across canvas "g"

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import BoardModifications.OvalModification;
import BoardModifications.PenModification;
import BoardModifications.RectModification;
import BoardModifications.TextModification;

public class SimpleGUI implements Runnable, WhiteboardGUI {

	private BufferedImage colorSample = new BufferedImage(

	buttonW, buttonH, BufferedImage.TYPE_INT_RGB);

	// object to facilitate listing remote whiteboard sessions, and logging

	// into one of them.

	// Current main drawing colour

	private Color currColor = Color.magenta;

	/* Pen width */

	// Status text bar

	private static int buttonW = 50;

	private static int buttonH = 50;

	private BufferedImage canvasImage;

	/** The main GUI */

	private JPanel gui;

	/** window and canvas dimensions */

	private static int canvasWidth = 800;

	private static int canvasHeight = 600;

	private static int windowWidth = 800;

	private static int windowHeight = 600;

	private BoardImage initImage;

	private JLabel imageLabel;

	private RenderingHints renderingHints;

	public String clientID = "";

	public SimpleGUI(BoardImage initImage) {

		this.initImage = initImage;

		SwingUtilities.invokeLater(this);

	}

	@Override
	public void run() {

		try {

			UIManager.setLookAndFeel(

			UIManager.getSystemLookAndFeelClassName());

		} catch (Exception e) {

			// use default

		}

		JFrame f = new JFrame(clientID);

		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		f.setLocationByPlatform(true);

		f.setContentPane(getGui());

		f.pack();

		f.setMinimumSize(f.getSize());

		// TODO SET FALSE

		f.setVisible(true);

		setImage(initImage.getImageFromPixels());

	}

	public JComponent getGui() {

		if (gui == null) {

			Map<Key, Object> hintsMap = new HashMap<RenderingHints.Key, Object>();

			hintsMap.put(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);

			hintsMap.put(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_ENABLE);

			hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

			renderingHints = new RenderingHints(hintsMap);

			// add in canvas.

			setImage(new BufferedImage(canvasWidth, canvasHeight,
					BufferedImage.TYPE_INT_RGB));

			gui = new JPanel((LayoutManager) new BorderLayout());

			JPanel imageView = new JPanel(new GridBagLayout());

			imageView
					.setPreferredSize(new Dimension(windowWidth, windowHeight));

			imageLabel = new JLabel(new ImageIcon(canvasImage));

			JScrollPane imageScroll = new JScrollPane(imageView);

			imageView.add(imageLabel);

			imageScroll.setBorder(null);

			gui.add(imageScroll, BorderLayout.CENTER);

		}

		return gui;

	}

	/**
	 * 
	 * This method will be invoked by the menu manager when the user
	 * 
	 * selects the "quit" menu item. It should terminate the application.
	 * 
	 * This method is required by the MenuCallback interface.
	 */

	public void shutdown() {

		System.exit(0);

	}

	/**
	 * 
	 * Callback defined by the MenuCallback interface to handle the user
	 * 
	 * requesting to change the pen color.
	 */

	public void changeColor() {

		Color c = JColorChooser.showDialog(

		gui, "Choose a color", currColor);

		if (c != null) {

			setColor(c);

		}

	}

	/** Set the current painting color and refresh any elements needed. */

	public void setColor(Color color) {

		this.currColor = color;

		setColorSample(color);

	}

	public void setColorSample(Color color) {

		Graphics2D g = colorSample.createGraphics();

		g.setRenderingHints(renderingHints);

		g.setColor(color);

		g.fillRect(0, 0, colorSample.getWidth(), colorSample.getHeight());

		g.dispose();

		imageLabel.repaint();

	}

	/**
	 * 
	 * Convert the canvas to a BoardImage that is suitable to be passed back to
	 * 
	 * the server via RMI calls.
	 */

	public BoardImage getCurrentBoardImage() {

		return new WhiteboardImage(canvasImage);

	}

	public void clear(BufferedImage bi) {

		Graphics2D g = bi.createGraphics();

		g.setRenderingHints(renderingHints);

		Color tempColor = g.getColor();

		g.setColor(Color.white);

		g.fillRect(0, 0, bi.getWidth(), bi.getHeight());

		g.setColor(tempColor);

		g.dispose();

		imageLabel.repaint();

	}

	public void setImage(BufferedImage image) {

		int w = image.getWidth();

		int h = image.getHeight();

		canvasImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = this.canvasImage.createGraphics();

		g.setRenderingHints(renderingHints);

		g.drawImage(image, 0, 0, gui);

		g.dispose();

		if (this.imageLabel != null) {

			imageLabel.setIcon(new ImageIcon(canvasImage));

			this.imageLabel.repaint();

		}

		if (gui != null) {

			gui.invalidate();

		}

	}

	// INDIVIDUAL UPDATE METHODS

	public void drawLine(int strokeSize, Color color, Point pointA, Point pointB) {

		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,

		BasicStroke.JOIN_ROUND, 1.7f);

		Graphics2D g = this.canvasImage.createGraphics();

		g.setRenderingHints(renderingHints);

		g.setColor(color);

		g.setStroke(tempStroke);

		if (pointB != null) {

			g.drawLine(pointA.x, pointA.y, pointB.x, pointB.y);

		} else {

			g.drawLine(pointA.x, pointA.y, pointA.x, pointA.y);

		}

		g.dispose();

		this.imageLabel.repaint();

	}

	public void drawRect(int strokeSize, Color color, Point pointA,

	Point pointB, boolean fill) {

		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,

		BasicStroke.JOIN_ROUND, 1.7f);

		// calculate top left corner and width height

		int x = Math.min(pointA.x, pointB.x);

		int y = Math.min(pointA.y, pointB.y);

		int width = Math.abs(pointA.x - pointB.x);

		int height = Math.abs(pointA.y - pointB.y);

		// add to canvas

		Graphics2D g = this.canvasImage.createGraphics();

		g.setRenderingHints(renderingHints);

		g.setColor(color);

		g.setStroke(tempStroke);

		g.drawRect(x, y, width, height);

		if (fill == true) {

			g.fillRect(x, y, width, height);

		}

		g.dispose();

		this.imageLabel.repaint();

	}

	public void drawOval(int strokeSize, Color color, Point pointA,

	Point pointB, boolean fill) {

		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,

		BasicStroke.JOIN_ROUND, 1.7f);

		// calculate top left corner and width height

		int x = Math.min(pointA.x, pointB.x);

		int y = Math.min(pointA.y, pointB.y);

		int width = Math.abs(pointA.x - pointB.x);

		int height = Math.abs(pointA.y - pointB.y);

		// add to canvas

		Graphics2D g = this.canvasImage.createGraphics();

		g.setRenderingHints(renderingHints);

		g.setColor(color);

		g.setStroke(tempStroke);

		g.drawOval(x, y, width, height);

		if (fill == true) {

			g.fillOval(x, y, width, height);

		}

		g.dispose();

		this.imageLabel.repaint();

	}

	public void drawString(String text, Color color, Point pointA, Point pointB) {

		// calculate top left corner and width height

		int x = Math.min(pointA.x, pointB.x);

		int y = Math.min(pointA.y, pointB.y);

		int width = Math.abs(pointA.x - pointB.x);

		int height = Math.abs(pointA.y - pointB.y);

		// add to canvas

		Graphics2D g = this.canvasImage.createGraphics();

		g.setRenderingHints(renderingHints);

		g.setColor(color);

		g.drawString(text, x, y);

		g.dispose();

		this.imageLabel.repaint();

	}

	/**
	 * 
	 * returns the current board image
	 * 
	 * 
	 * 
	 * @return
	 */

	public BoardImage getBoardImage() {

		return new WhiteboardImage(canvasImage);

	}

	/*
	 * 
	 * Reset the board image
	 */

	public void setBoardImage(BoardImage image) {

		setImage(image.getImageFromPixels());

	}

	public void updateBoard(BoardModification mod, String updatingUser) {

		if (mod instanceof PenModification) {

			PenModification penMod = (PenModification) mod;

			drawLine(penMod.strokeSize, penMod.color,

			penMod.pointA, penMod.pointB);

		}

		if (mod instanceof OvalModification) {

			OvalModification ovalMod = (OvalModification) mod;

			drawOval(ovalMod.strokeSize, ovalMod.color,

			ovalMod.pointA, ovalMod.pointB, ovalMod.fill);

		}

		if (mod instanceof RectModification) {

			RectModification rectMod = (RectModification) mod;

			drawRect(rectMod.strokeSize, rectMod.color,

			rectMod.pointA, rectMod.pointB, rectMod.fill);

		}

		if (mod instanceof TextModification) {

			TextModification textMod = (TextModification) mod;

			drawString(textMod.text, textMod.color, textMod.pointA,

			textMod.pointB);

		}

	}

}

// vim: ts=4 sw=4 et

