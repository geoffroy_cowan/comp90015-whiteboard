package client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.LinkedList;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import server.ClientServant;
import server.CryptographicSalt;
import server.RemoteLogin;
import server.exceptions.AccountDetailsTooLongException;
import server.exceptions.AuthenticationException;
import server.exceptions.CreateAccountException;
import server.exceptions.DetailsNotRegisteredException;
import server.exceptions.LoginException;


public final class SimpleClientLogin implements ClientLogin
{
    private static final int LOGIN_RETRIES = 10;
    public static final String CIPHER_ALGORITHM = "RSA";
    
    private RemoteLogin loginObject;
    private ClientCredentials credentials;
    private char [] password;
    private ClientMessenger messenger;
    private PublicKey serverPK;
    
    /*
     * constructor providing required details needed for login
     */
    public SimpleClientLogin(String servername, int port)
            throws RemoteException, NotBoundException
    {
        //obtain reference to server RMI registry
        //System.out.println("connecting to server at " + servername);
        Registry reg = LocateRegistry.getRegistry(servername, port);
        
        //lookup login object in registry and obtain login object
        this.loginObject
            = (RemoteLogin)reg.lookup(ClientLogin.loginServerName);
        this.credentials = null;
        this.password = null;
        this.messenger = null;
        this.serverPK = loginObject.getPublicKey();
    }
    
    /*
     * re-saves the user's username/password
     */
    public void registerCredentials(String username, char [] password)
    {
        this.credentials = new ClientCredentials ();
        this.credentials.username = username;
        this.password = password;
    }
    
    /*
     * re-saves the user's ClientReceiver
     */
    public void registerReceiver(ClientReceiver receiver)
            throws RemoteException
    {
        ClientMessenger messenger = new SimpleClientMessenger(receiver);
        this.messenger = messenger;
    }
    

    /*
     * method to perform login to existing whiteboard instance on server
     */
    public ClientServant
            loginToExisting(String whiteboardName)
            throws RemoteException, LoginException
    {
        if (password == null || messenger == null)
            throw new DetailsNotRegisteredException();

        CryptographicSalt salt = loginObject.getCurrentSalt ();
        credentials.digest = PasswordDigest.getDigest (password, salt);
        credentials.salt = salt;
        ClientServant session = loginObject.loginToExisting (credentials, 
                whiteboardName, messenger);

        return session;
    }


    /*
     * method to perform login to new whiteboard instance on server
     */
    public ClientServant
            loginToNew(String whiteBoardName, BoardImage startingImage)
            throws RemoteException, LoginException
    {
        if (password == null || messenger == null)
            throw new DetailsNotRegisteredException();

        CryptographicSalt salt = loginObject.getCurrentSalt ();
        //System.out.println("returned salt: " + Arrays.toString(salt.contents()));
        credentials.digest = PasswordDigest.getDigest (password, salt);
        credentials.salt = salt;
        //System.out.println("maybe altered salt: " + Arrays.toString(credentials.salt.contents()));
        ClientServant session = loginObject.loginToNew (credentials, 
                whiteBoardName, messenger, startingImage);

        return session;
    }



    /*
     * obtain from RemoteSetup object the current list of whiteboard instances
     */
    public LinkedList<String>
            listWhiteboardInstances()
            throws RemoteException
    {
        return loginObject.listWhiteboards();
    }
    
    /*
     * Attempts to create an account on server with supplied username/password
     */
    public void
            createAccount()
            throws RemoteException, CreateAccountException
    {
        //first convert credentials to a byte array
        byte[] detailsBytes;
        byte [] encryptedDetails;
        try
        {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream objectStream = new ObjectOutputStream(byteStream);
            AccountDetails details =
                    new AccountDetails(credentials.username, password);
            objectStream.writeObject(details);
            detailsBytes = byteStream.toByteArray();
        }
        catch (IOException e)
        {
            //failure in creating byte array from credentials
            throw new CreateAccountException("failed to serialize credentials");
        }
        try
        {
            final Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, serverPK);
            //System.out.println((new Integer(detailsBytes.length).toString()));
            encryptedDetails = cipher.doFinal(detailsBytes);
        }
        catch (InvalidKeyException e)
        {
            throw new CreateAccountException("supplied public key invalid");
        }
        catch (IllegalBlockSizeException e)
        {
            throw new AccountDetailsTooLongException();
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace();
            throw new CreateAccountException("failure in using"
                    + " requested encryption algorithm");
        }
        
        loginObject.createAccount(encryptedDetails);
    }



}
