package client;

/*
 * Interface for a general GUI object containing a whiteboard image
 * 
 * must export methods to update using a BoardModification, and to
 * obtain a BoardImage object representing current visual state of board
 */
public interface WhiteboardGUI
{
    /*
     * method to update board with supplied modification
     */
    public void updateBoard(BoardModification newModification, String userUpdating);
    
    /*
     * method to return current BoardImage object representing
     * current state of board
     */
    public BoardImage getBoardImage();
    
    /*
     * method to set the image using a supplied BoardImage
     */
    public void setBoardImage(BoardImage newImage);
}
