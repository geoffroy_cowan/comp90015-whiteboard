package client;

import BoardModifications.*;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.text.StyleConstants;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import server.ClientServant;
import server.exceptions.ServantException;
import BoardModifications.ChatModification;
import BoardModifications.PenModification;

public class ClientGUI implements MenuCallback, Runnable {
	private int selfSync = 0;
	private String myName, whiteboardName = "";
	private String serverName;
	private BufferedImage colorSample = new BufferedImage(buttonW, buttonH,
			BufferedImage.TYPE_INT_RGB);

	/** Object for sending messages to server */
	private ClientServant clientServant = null;
	private ClientReceiver receiver;
	
	private boolean loggedOff = true;

	/**
	 * object to facilitate listing remote whiteboard sessions, and logging into
	 * one of them.
	 */
	private ClientLogin loginServer = null;
	private boolean isAdmin;
	// Current main drawing colour
	private Color currColor = Color.magenta;
	public boolean fill = true;

	// Active tool
	private int currTool = Tools.PEN;

	// Last drawn point, helps filling gaps in quickly drawn lines
	private Point lastPoint = null;

	/* Pen width */
	private int strokeSize = 3;
	private static int maxStrokeSize = 32;

	// Status text bar
	private static int buttonW = 50;
	private static int buttonH = 50;
	// Actual whiteboard image
	private BufferedImage canvasImage;
	// Main Gui
	private JPanel gui;

	/** window and canvas dimensions */
	private static int canvasWidth = 800;
	private static int canvasHeight = 550;
	private static int windowWidth = 800;
	private static int windowHeight = 525;

	/** text fields that require updating */
	private JTextField chatEntry;
	private JTextArea chatLog;
	private JTextPane userList;
	private JFrame f;

	private JLabel imageLabel;
	private Stroke stroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,
			BasicStroke.JOIN_ROUND, 1.7f);
	private RenderingHints renderingHints;
	public String clientID = "";

	private ClientLogin clientLoginObject = null;
	private String currAdmin = "";

	public ClientGUI(ClientReceiver clientReceiver, String ID, String username) {
		clientID = ID;
		receiver = clientReceiver;
		SwingUtilities.invokeLater(this);
		this.myName = username;
	}

	@Override
	public void run() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// use default
		}

		f = new JFrame(clientID);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setLocationByPlatform(true);
		f.setContentPane(getGui());
		MenuManager.initMenuBar(f, this);

		f.pack();
		f.setMinimumSize(f.getSize());
		f.setVisible(true);
		f.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
	            shutdown();
	        }	
		});
	}

	public JComponent getGui() {
		if (gui == null) {
			Map<Key, Object> hintsMap = new HashMap<RenderingHints.Key, Object>();
			hintsMap.put(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			hintsMap.put(RenderingHints.KEY_DITHERING,
					RenderingHints.VALUE_DITHER_ENABLE);
			hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING,
					RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			renderingHints = new RenderingHints(hintsMap);

			// add in canvas.
			setImage(new BufferedImage(canvasWidth, canvasHeight,
					BufferedImage.TYPE_INT_RGB));
			gui = new JPanel((LayoutManager) new BorderLayout());
			JPanel imageView = new JPanel(new GridBagLayout());
			imageView
					.setPreferredSize(new Dimension(windowWidth, windowHeight));
			imageLabel = new JLabel(new ImageIcon(canvasImage));
			JScrollPane imageScroll = new JScrollPane(imageView);
			imageView.add(imageLabel);
			imageLabel
					.addMouseMotionListener(new ClientImageMouseMotionListener());
			imageLabel.addMouseListener(new ClientImageMouseListener());
			imageScroll.setBorder(null);
			gui.add(imageScroll, BorderLayout.CENTER);

			// add toolbar.
			JToolBar tools = new JToolBar(JToolBar.VERTICAL);
			tools.setFloatable(false);

			// add chat toolbar
			chatEntry = new JTextField();
			chatEntry.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO get name properly
					String name = myName;
					SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss.SSS");
					Date now = new Date();
					String timeString = sdf.format(now);
					sendChat(name, timeString, chatEntry.getText());
					chatEntry.setText("");
				}
			});

			chatLog = new JTextArea();
			userList = new JTextPane();
			userList.setPreferredSize(new Dimension(60, 200));
			// userList.setColumns(30);
			chatLog.setColumns(25);
			chatLog.setLineWrap(true);
			// userList.setLineWrap(true);
			userList.setEditable(false);
			chatLog.setEditable(false);
			chatEntry.setColumns(25);
			chatLog.setRows(20);
			// userList.setRows(10);
			JToolBar chatBar = new JToolBar(JToolBar.VERTICAL);
			chatBar.setFloatable(false);
			JScrollPane userListPane = new JScrollPane(userList);
			userListPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			JScrollPane chatLogPane = new JScrollPane(chatLog);
			chatLogPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			chatBar.add(userListPane);
			chatBar.add(chatLogPane);
			chatBar.add(chatEntry);
			gui.add(chatBar, BorderLayout.EAST);

			// create colour button
			JButton colorButton = new JButton();
			colorButton.setMnemonic('o');
			colorButton.setToolTipText("Choose a Color");
			// create listener
			ActionListener colorListener = new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					changeColor();
				}
			};
			// attach listener to button
			colorButton.addActionListener(colorListener);
			colorButton.setIcon(new ImageIcon(colorSample));
			//colorButton.setBorder(null);
			// add button to toolbar.
			tools.add(colorButton);
			// Initialise button colour
			setColor(currColor);

			// create colour button
			final JButton thicknessButton = new JButton(new ImageIcon(
					((new ImageIcon("assets/lines.png")).getImage())
							.getScaledInstance(buttonW, buttonH,
									java.awt.Image.SCALE_SMOOTH)));
			thicknessButton.setToolTipText("Choose a pen thickness");
			// create listener
			ActionListener thicknessListener = new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					final SpinnerNumberModel strokeModel = new SpinnerNumberModel(
							strokeSize, 1, maxStrokeSize, 1);
					JSpinner spinner = new JSpinner(strokeModel);
					int option = JOptionPane.showOptionDialog(null, spinner,
							"Pen width... (1-" + maxStrokeSize + ")",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null, null, null);
					if (option == JOptionPane.CANCEL_OPTION) {
						// user hit cancel - do nothing
					} else if (option == JOptionPane.OK_OPTION) {
						// user entered a number - set new thickness
						Object o = spinner.getValue();
						Integer i = (Integer) o;
						strokeSize = i.intValue();
						stroke = new BasicStroke(i.intValue(),
								BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
								1.7f);
					}
				}
			};
			// attach listener to button
			thicknessButton.addActionListener(thicknessListener);
			// add button to toolbar.

			// create buttons, set images.
			final JButton pen = new JButton(new ImageIcon(((new ImageIcon(
					"assets/pencil.png")).getImage()).getScaledInstance(
					buttonW, buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton rect = new JButton(new ImageIcon(((new ImageIcon(
					"assets/rectangle.png")).getImage()).getScaledInstance(
					buttonW, buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton circle = new JButton(new ImageIcon(((new ImageIcon(
					"assets/elipse.png")).getImage()).getScaledInstance(
					buttonW, buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton text = new JButton(new ImageIcon(((new ImageIcon(
					"assets/text.png")).getImage()).getScaledInstance(buttonW,
					buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton fillE = new JButton(new ImageIcon(((new ImageIcon(
					"assets/fill_true.png")).getImage()).getScaledInstance(
					buttonW, buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton eraser = new JButton(new ImageIcon(((new ImageIcon(
					"assets/eraser.png")).getImage()).getScaledInstance(
					buttonW, buttonH, java.awt.Image.SCALE_SMOOTH)));
			final JButton line = new JButton(new ImageIcon(((new ImageIcon(
					"assets/line.png")).getImage()).getScaledInstance(buttonW,
					buttonH, java.awt.Image.SCALE_SMOOTH)));
			// pen.setBorder(null);
			// rect.setBorder(null);
			// circle.setBorder(null);
			// text.setBorder(null);
			// fillE.setBorder(null);
			// eraser.setBorder(null);
			// line.setBorder(null);
			// thicknessButton.setBorder(null);

			// add buttons to toolbar
			tools.add(pen);
			tools.add(line);
			tools.add(rect);
			tools.add(circle);
			tools.add(text);
			tools.add(eraser);
			tools.add(thicknessButton);
			tools.add(fillE);

			// add to group so you cant have more than one active at once
			ButtonGroup toolGroup = new ButtonGroup();
			toolGroup.add(pen);
			toolGroup.add(rect);
			toolGroup.add(circle);
			toolGroup.add(text);
			toolGroup.add(fillE);
			toolGroup.add(eraser);
			toolGroup.add(line);

			// add listener to group
			ActionListener toolGroupListener = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					if (ae.getSource() == pen) {
						currTool = Tools.PEN;
					} else if (ae.getSource() == rect) {
						currTool = Tools.RECT;
					} else if (ae.getSource() == circle) {
						currTool = Tools.CIRCLE;
					} else if (ae.getSource() == text) {
						currTool = Tools.TEXT;
					} else if (ae.getSource() == fillE) {
						if (fill == true) {
							fill = false;
							fillE.setIcon(new ImageIcon(((new ImageIcon(
									"assets/fill_false.png")).getImage())
									.getScaledInstance(buttonW, buttonH,
											java.awt.Image.SCALE_SMOOTH)));
						} else {
							fill = true;
							fillE.setIcon(new ImageIcon(((new ImageIcon(
									"assets/fill_true.png")).getImage())
									.getScaledInstance(buttonW, buttonH,
											java.awt.Image.SCALE_SMOOTH)));
						}
					} else if (ae.getSource() == eraser) {
						currTool = Tools.ERASER;
					} else if (ae.getSource() == line) {
						currTool = Tools.LINE;
					}
				}
			};
			pen.addActionListener(toolGroupListener);
			rect.addActionListener(toolGroupListener);
			circle.addActionListener(toolGroupListener);
			text.addActionListener(toolGroupListener);
			fillE.addActionListener(toolGroupListener);
			eraser.addActionListener(toolGroupListener);
			line.addActionListener(toolGroupListener);
			tools.setBackground(Color.WHITE);

			gui.add(tools, BorderLayout.WEST);

			setColorSample(currColor);
			clear(canvasImage);
		}
		return gui;
	}

	/**
	 * This method will be invoked by the menu manager when the user selects the
	 * "quit" menu item. It should terminate the application. This method is
	 * required by the MenuCallback interface.
	 * @throws  
	 */
	public void shutdown() {
	    this.logOff();
		System.exit(0);
	}

    /**
     * This method should be called when we wish to log off correctly from
     * current whiteboard instance
     * @throws  
     */
    public void logOff() {
        if (clientServant != null) {
            
            //append event to chatlog
            SimpleDateFormat sdf = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            Date now = new Date();
            String timeString = sdf.format(now);
            chatLog.append("*********************************************\n");
            chatLog.append("[" + timeString + "] logging off from whiteboard\n"
                    + "\"" + whiteboardName  + "\"\n");
            userList.setText("");
            try
            {
                loggedOff = true;
                whiteboardName = "";
                clientServant.logout();
                //wait for logout to occur from server and clientServant to become false
                while (true)
                {
                    //synchronized access to clientServant
                    synchronized(this)
                    {
                        if (clientServant == null)
                            break;
                    }
                }
            }
            catch (RemoteException e)
            {
                //shouldn't happen
                e.printStackTrace();
            }
        }
    }
	
	/**
	 * Set clientServant used for sending updates
	 * 
	 * @param clientServant
	 */
	public void setClientServant(ClientServant clientServant) {
	    

	    
	    if (this.clientServant != null)
	        this.logOff();
		this.clientServant = clientServant;
        this.loggedOff = false;
		updateUsers();
		
        //append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] logged into whiteboard\n"
                + "\"" + whiteboardName + "\" as \"" + myName + "\"\n");
        chatLog.append("*********************************************\n");
		
	}

	/**
	 * Set username to be used for comparison against incoming updates
	 */
	public void setUserName(String username) {
		myName = username;
	}
	
	/**
	 * sets name of whiteboard we are logged on to
	 * @param whiteboardName
	 */
	public void setWhiteboardName(String whiteboardName) {
	    this.whiteboardName = whiteboardName;
	}

	/**
	 * Callback defined by the MenuCallback interface to handle the user
	 * requesting to change the pen color.
	 */
	public void changeColor() {
		Color c = JColorChooser.showDialog(gui, "Choose a color", currColor);

		if (c != null) {
			setColor(c);
		}
	}

	/**
	 * MenuCallback method to handle saving the current whiteboard state into an
	 * image file on the users local computer.
	 */
	public void saveBoard() {
		File imageFile;
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter pngOnly = new FileNameExtensionFilter(
				"PNG images", "png");

		fileChooser.setFileFilter(pngOnly);

		// prompt the user to select a file name.
		if (fileChooser.showSaveDialog(gui) == JFileChooser.APPROVE_OPTION) {
			imageFile = fileChooser.getSelectedFile();

			try {
				ImageIO.write(canvasImage, "png", imageFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
    /**
     * sets blank canvas
     * (as in loadFromFile requires admin privelages if logged in to a whiteboard
     * session)
     */
    public void createNew()
    {
        synchronized(this)
        {
            
            
            if (!loggedOff && !myName.equals(currAdmin))
            {
                this.displayErrorMessage("Error: This action requires\n"
                        + "administrator privelages!");
                return;
            }
        }
        
        this.clear(canvasImage);
        
        //send image to peers if appropriate
        if (!loggedOff && myName.equals(currAdmin))
        {
            BoardImage imageToSend = new WhiteboardImage(canvasImage);
            try
            {
                clientServant.setBoardImage(imageToSend);
            }
            catch (RemoteException e)
            {
                //should never occur...
                e.printStackTrace();
            }
            catch (ServantException e)
            {
                //should never occur...
                e.printStackTrace();
            }
        }
        
    }

	/**
	 * Load the whiteboard with an initial image stored in a local file. This
	 * method is called when the user selects the open menu item. TODO: Do we
	 * update the server?
	 */
	public void loadFromFile() {
	    synchronized(this)
	    {
	        if (!loggedOff && !myName.equals(currAdmin))
	        {
                this.displayErrorMessage("Error: This action requires\n"
                        + "administrator privelages!");
                return;
	        }
	    }
		File imageFile;
		BufferedImage newImage;
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter pngOnly = new FileNameExtensionFilter(
				"PNG images", "png");

		fileChooser.setFileFilter(pngOnly);

		// prompt the user to select a file name.
		if (fileChooser.showSaveDialog(gui) == JFileChooser.APPROVE_OPTION) {
			imageFile = fileChooser.getSelectedFile();

			try {
				// read an image from the file selected by the user, and
				// set it as the whiteboard contents.
				newImage = ImageIO.read(imageFile);
				setImage(newImage);
			    if (!loggedOff && myName.equals(currAdmin))
			    {
			        BoardImage imageToSend = new WhiteboardImage(newImage);
			        clientServant.setBoardImage(imageToSend);
			    }
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Allow the user to select a server to connect to, and to enter their login
	 * credentials via a dialog window. This method will set up the loginServer
	 * member variable. Once that is done, we need to display a list of
	 * available sessions, and get the user to select one. Then we log in to it.
	 */
	public void selectServer() {
		WhiteboardSelectionDialogue.startWizard(this, whiteboardName);
	}

	public void setupConnectionDetails() {
		SetupConnectionDetailsDialogue.startWizard(this, receiver);
	}

	public void createNewAccount() {
		CreateAccountDialogue.startWizard(this);
	}
	
    public void kickPeer()
    {
        boolean canKick;
        synchronized (this)
        {
            if (loggedOff || !myName.equals(currAdmin))
                canKick = false;
            else
                canKick = true;
        }
        KickPeerDialogue.startWizard(canKick, clientServant, myName);
    }
    
    public void promotePeer()
    {
        boolean canPromote;
        synchronized (this)
        {
            if (loggedOff || !myName.equals(currAdmin))
                canPromote = false;
            else
                canPromote = true;
        }
        PromotePeerDialogue.startWizard(canPromote, clientServant, myName);
    }

    public void askLogoff()
    {
        AskLogoffDialogue.startWizard(this, loggedOff);
    }

	/** Set the current painting color and refresh any elements needed. */
	public void setColor(Color color) {
		this.currColor = color;
		setColorSample(color);
	}

	/**
	 * Sets the current colour preview square
	 * 
	 * @param color
	 */
	public void setColorSample(Color color) {
		Graphics2D g = colorSample.createGraphics();
		g.setRenderingHints(renderingHints);
		g.setColor(color);
		g.fillRect(0, 0, colorSample.getWidth(), colorSample.getHeight());
		g.dispose();
		imageLabel.repaint();
	}

	/**
	 * fills an image to white
	 * 
	 * @param bi
	 */
	public void clear(BufferedImage bi) {
		Graphics2D g = bi.createGraphics();
		g.setRenderingHints(renderingHints);
		Color tempColor = g.getColor();
		g.setColor(Color.white);
		g.fillRect(0, 0, bi.getWidth(), bi.getHeight());
		g.setColor(tempColor);
		g.dispose();
		imageLabel.repaint();
	}

	/**
	 * Convert the canvas to a BoardImage that is suitable to be passed back to
	 * the server via RMI calls.
	 */
	public BoardImage getCurrentBoardImage() {
		return new WhiteboardImage(canvasImage);
	}

	/**
	 * returns the current board image
	 * 
	 * @return
	 */
	public BoardImage getBoardImage() {
		return new WhiteboardImage(canvasImage);
	}

	/**
	 * Set the initial image from the BoardImage sent from the remote server.
	 */
	public void setBoardImage(BoardImage image) {
		setImage(image.getImageFromPixels());
	}

	/**
	 * Draws image to canvas
	 * 
	 * @param image
	 */
	public void setImage(BufferedImage image) {
		int w = image.getWidth();
		int h = image.getHeight();
		canvasImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = this.canvasImage.createGraphics();
		g.setRenderingHints(renderingHints);
		g.drawImage(image, 0, 0, gui);
		g.dispose();

		if (this.imageLabel != null) {
			imageLabel.setIcon(new ImageIcon(canvasImage));
			this.imageLabel.repaint();
		}
		if (gui != null) {
			gui.invalidate();
		}
	}

	// INDIVIDUAL UPDATE METHODS
	public synchronized void drawLine(int strokeSize, Color color, Point pointA, Point pointB) {
		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND, 1.7f);
		Graphics2D g = this.canvasImage.createGraphics();
		g.setRenderingHints(renderingHints);
		g.setColor(color);
		g.setStroke(tempStroke);
		if (pointB != null) {
			g.drawLine(pointA.x, pointA.y, pointB.x, pointB.y);
		} else {
			g.drawLine(pointA.x, pointA.y, pointA.x, pointA.y);
		}
		g.dispose();
		this.imageLabel.repaint();
	}

	public synchronized void drawRect(int strokeSize, Color color, Point pointA,
			Point pointB, boolean fill) {
		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND, 1.7f);
		// calculate top left corner and width height
		int x = Math.min(pointA.x, pointB.x);
		int y = Math.min(pointA.y, pointB.y);
		int width = Math.abs(pointA.x - pointB.x);
		int height = Math.abs(pointA.y - pointB.y);
		// add to canvas
		Graphics2D g = this.canvasImage.createGraphics();
		g.setRenderingHints(renderingHints);
		g.setColor(color);
		g.setStroke(tempStroke);
		g.drawRect(x, y, width, height);
		if (fill == true) {
			g.fillRect(x, y, width, height);
		}
		g.dispose();
		this.imageLabel.repaint();
	}

	public synchronized void drawOval(int strokeSize, Color color, Point pointA,
			Point pointB, boolean fill) {
		Stroke tempStroke = new BasicStroke(strokeSize, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND, 1.7f);
		// calculate top left corner and width height
		int x = Math.min(pointA.x, pointB.x);
		int y = Math.min(pointA.y, pointB.y);
		int width = Math.abs(pointA.x - pointB.x);
		int height = Math.abs(pointA.y - pointB.y);
		// add to canvas
		Graphics2D g = this.canvasImage.createGraphics();
		g.setRenderingHints(renderingHints);
		g.setColor(color);
		g.setStroke(tempStroke);
		g.drawOval(x, y, width, height);
		if (fill == true) {
			g.fillOval(x, y, width, height);
		}
		g.dispose();
		this.imageLabel.repaint();
	}

	public synchronized void drawString(String text, Color color, Point pointA, Point pointB) {
		// calculate top left corner and width height
		int x = Math.min(pointA.x, pointB.x);
		int y = Math.min(pointA.y, pointB.y);
		int width = Math.abs(pointA.x - pointB.x);
		int height = Math.abs(pointA.y - pointB.y);
		// add to canvas
		Graphics2D g = this.canvasImage.createGraphics();
		g.setRenderingHints(renderingHints);
		g.setColor(color);
		g.drawString(text, x, y);
		g.dispose();
		this.imageLabel.repaint();
	}

	public synchronized void drawChat(String author, String timestamp, String message) {
		String formattedTime = timestamp.substring(11,16);
		String chatMessage = "[" + formattedTime + "] " + author + " says:\n" + message + "\n";
		chatLog.append(chatMessage);
	}

	// INDIVIDUAL SEND METHODS
	public void sendChat(String author, String timestamp, String message) {
		ChatModification mod = new ChatModification(author, timestamp, message);
		sendUpdate(mod);
	}

	public void sendLine(int stroke, Color color, Point pointA, Point pointB) {
		PenModification mod = new PenModification(strokeSize, color, pointA,
				pointB);
		sendUpdate(mod);
	}

	public void sendRect(int strokeSize, Color color, Point pointA,
			Point pointB, boolean fill) {
		RectModification mod = new RectModification(strokeSize, color, pointA,
				pointB, fill);
		sendUpdate(mod);
	}

	public void sendOval(int strokeSize, Color color, Point pointA,
			Point pointB, boolean fill) {
		OvalModification mod = new OvalModification(strokeSize, color, pointA,
				pointB, fill);
		sendUpdate(mod);
	}

	public void sendText(String text, Color color, Point pointA, Point pointB) {
		TextModification mod = new TextModification(text, color, pointA, pointB);
		sendUpdate(mod);
	}

	// SEND CONSTRUCTED MODIFICATION TO SERVANT
	private void sendUpdate(BoardModification mod) {
		try {
			if (clientServant != null) {
				clientServant.updateBoard(mod);
				selfSync++;
			}
			else {
			    this.updateBoard(mod, myName);
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServantException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// HANDLE INCOMING MODIFICATIONS
	public synchronized void updateBoard(BoardModification mod, String updatingUser) {
		//if (updatingUser != null && myName != null) {
			/*if (updatingUser.equals(myName)) {
				selfSync--;
				if (mod instanceof ChatModification) {
					ChatModification chatMod = (ChatModification) mod;
					drawChat(chatMod.author, chatMod.timestamp, chatMod.message);
				}
			} else {
				if (selfSync == 0) {*/
					if (mod instanceof PenModification) {
						PenModification penMod = (PenModification) mod;
						drawLine(penMod.strokeSize, penMod.color,
								penMod.pointA, penMod.pointB);
					}
					if (mod instanceof ChatModification) {
						ChatModification chatMod = (ChatModification) mod;
						drawChat(chatMod.author, chatMod.timestamp,
								chatMod.message);
					}
					if (mod instanceof OvalModification) {
						OvalModification ovalMod = (OvalModification) mod;
						drawOval(ovalMod.strokeSize, ovalMod.color,
								ovalMod.pointA, ovalMod.pointB, ovalMod.fill);
					}
					if (mod instanceof RectModification) {
						RectModification rectMod = (RectModification) mod;
						drawRect(rectMod.strokeSize, rectMod.color,
								rectMod.pointA, rectMod.pointB, rectMod.fill);
					}
					if (mod instanceof TextModification) {
						TextModification textMod = (TextModification) mod;
						drawString(textMod.text, textMod.color, textMod.pointA,
								textMod.pointB);
					}
				/*} else {
					try {
						setImage(clientServant.getBoardImage()
								.getImageFromPixels());
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}*/
		//}
	}

	// Handle client recevier methods-
	public void updateUsers() {
		SimpleAttributeSet sas = new SimpleAttributeSet();
		try {
			String admin = clientServant.getAdminName();
			currAdmin = admin;
			LinkedList<String> clients = clientServant.getClientList();
			//System.out.println("" + clients.size());
			userList.setText("");
			for (int i = 0; i < clients.size(); i++) {
				String cUser = clients.get(i);
				userList.setText(userList.getText() + cUser + "\n");
			}
			int start = userList.getText().indexOf(admin);
			int length = admin.length();
			StyleConstants.setBold(sas, true);
			userList.getStyledDocument().setCharacterAttributes(start, length,
					sas, false);
			int meStart = userList.getText().indexOf(myName);
			int meLength = myName.length();
			StyleConstants.setForeground(sas, Color.GREEN);
			userList.getStyledDocument().setCharacterAttributes(meStart, meLength,
					sas, false);


		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addUser(String newUser) {
		if (currAdmin == "") {
			currAdmin = myName;
		}
		
		//append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] new user\n"
                + "\"" + newUser + "\" logged in\n");
		
		userList.setText(userList.getText() + newUser + "\n");
		SimpleAttributeSet sas = new SimpleAttributeSet();
		int start = userList.getText().indexOf(currAdmin);
		int length = currAdmin.length();
		StyleConstants.setBold(sas, true);
		userList.getStyledDocument().setCharacterAttributes(start, length, sas,
				false);
		int meStart = userList.getText().indexOf(myName);
		int meLength = myName.length();
		StyleConstants.setForeground(sas, Color.GREEN);
		userList.getStyledDocument().setCharacterAttributes(meStart, meLength,
				sas, false);

	}

	public void removeUser(String oldUser) {
		if (currAdmin == "") {
			currAdmin = myName;
		}
		
        //append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] user\n"
                + "\"" + oldUser + "\" left\n");
		
		userList.setText(userList.getText().replace(oldUser + "\n", ""));
		SimpleAttributeSet sas = new SimpleAttributeSet();
		int start = userList.getText().indexOf(currAdmin);
		int length = currAdmin.length();
		StyleConstants.setBold(sas, true);
		userList.getStyledDocument().setCharacterAttributes(start, length, sas,
				false);
		int meStart = userList.getText().indexOf(myName);
		int meLength = myName.length();
		StyleConstants.setForeground(sas, Color.GREEN);
		userList.getStyledDocument().setCharacterAttributes(meStart, meLength,
				sas, false);
	}

	public void newAdmin(String newAdmin) {
	    
        //append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] user\n"
                + "\"" + newAdmin + "\" promoted to admin\n");
	    
		currAdmin = newAdmin;
		userList.setText(userList.getText());
		SimpleAttributeSet sas = new SimpleAttributeSet();
		int start = userList.getText().indexOf(currAdmin);
		int length = currAdmin.length();
		StyleConstants.setBold(sas, true);
		userList.getStyledDocument().setCharacterAttributes(start, length, sas,
				false);
		if (currAdmin == myName) {
			isAdmin = true;
		}
		int meStart = userList.getText().indexOf(myName);
		int meLength = myName.length();
		StyleConstants.setForeground(sas, Color.GREEN);
		userList.getStyledDocument().setCharacterAttributes(meStart, meLength,
				sas, false);
	}

	public boolean getPermission(String user) {
		int result = JOptionPane.showConfirmDialog(this.gui, user
				+ " wants to join. Is this ok?", "Permission",
				JOptionPane.INFORMATION_MESSAGE);
		return (result == JOptionPane.OK_OPTION);
	}

	public synchronized void kick() {
	    if (loggedOff == false)
	    {
	        this.displayErrorMessage("You have been kicked\n"
	                + "from current whiteboard session.");
            //append event to chatlog
            SimpleDateFormat sdf = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            Date now = new Date();
            String timeString = sdf.format(now);
            chatLog.append("*********************************************\n");
            chatLog.append("[" + timeString + "] logging off from whiteboard\n"
                    + "\"" + whiteboardName  + "\"\n");
            userList.setText("");
	    }
		clientServant = null;
		loggedOff = true;
		whiteboardName = "";
	}

	public void setTitle(String newTitle) {
		f.setTitle(newTitle);
	}

	// MOUSE CLICK LISTENERS
	class ClientImageMouseListener extends MouseAdapter {
		// MOUSE - ON CLICK
		@Override
		public void mousePressed(MouseEvent arg0) {
			if (currTool == Tools.PEN) {
				//drawLine(strokeSize, currColor, arg0.getPoint(), null);
				sendLine(strokeSize, currColor, arg0.getPoint(), null);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.RECT) {
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.CIRCLE) {
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.TEXT) {
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.ERASER) {
				//drawLine(strokeSize, Color.WHITE, arg0.getPoint(), null);
				sendLine(strokeSize, Color.WHITE, arg0.getPoint(), null);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.LINE) {
				lastPoint = arg0.getPoint();
			} else {
				// ERROR
			}
		}

		// MOUSE - ON RELEASE
		@Override
		public void mouseReleased(MouseEvent arg0) {
			if (currTool == Tools.PEN) {
				//drawLine(strokeSize, currColor, arg0.getPoint(), null);
				sendLine(strokeSize, currColor, arg0.getPoint(), null);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.RECT) {
				//drawRect(strokeSize, currColor, arg0.getPoint(), lastPoint,
				//		fill);
				sendRect(strokeSize, currColor, arg0.getPoint(), lastPoint,
						fill);
			} else if (currTool == Tools.CIRCLE) {
				//drawOval(strokeSize, currColor, arg0.getPoint(), lastPoint,
				//		fill);
				sendOval(strokeSize, currColor, arg0.getPoint(), lastPoint,
						fill);
			} else if (currTool == Tools.TEXT) {
				String text = JOptionPane.showInputDialog("enter text");
				//drawString(text, currColor, arg0.getPoint(), lastPoint);
				sendText(text, currColor, arg0.getPoint(), lastPoint);
			} else if (currTool == Tools.ERASER) {
				//drawLine(strokeSize, Color.WHITE, arg0.getPoint(), null);
				sendLine(strokeSize, Color.WHITE, arg0.getPoint(), null);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.LINE) {
				//drawLine(strokeSize, currColor, arg0.getPoint(), lastPoint);
				sendLine(strokeSize, currColor, arg0.getPoint(), lastPoint);
			} else {
				// ERROR
			}
		}
	}

	// MOUSE MOVEMENT LISTENERS
	class ClientImageMouseMotionListener implements MouseMotionListener {
		// MOUSE - ON DRAG
		@Override
		public void mouseDragged(MouseEvent arg0) {
			if (currTool == Tools.PEN) {
				//drawLine(strokeSize, currColor, arg0.getPoint(), lastPoint);
				sendLine(strokeSize, currColor, arg0.getPoint(), lastPoint);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.RECT) {
				// TODO
				/**
				 * draw temporary transparent shaded rect to show position
				 * before release where temp rect is deleted/ hidden and final
				 * is drawn.
				 */
			} else if (currTool == Tools.ERASER) {
				//drawLine(strokeSize, Color.WHITE, arg0.getPoint(), lastPoint);
				sendLine(strokeSize, Color.WHITE, arg0.getPoint(), lastPoint);
				lastPoint = arg0.getPoint();
			} else if (currTool == Tools.CIRCLE) {
				// TODO
			} else if (currTool == Tools.TEXT) {
				// TODO
			} else {
				// ERROR
			}

		}

		// MOUSE - ON MOVE
		@Override
		public void mouseMoved(MouseEvent arg0) {
			// nothing....
		}
	}

	/**
	 * obtains the local ClientLogin object
	 * 
	 * @return ClientLogin object
	 */
	public ClientLogin getClientLogin() {
		return this.clientLoginObject;
	}

	/**
	 * sets the local ClientLogin object
	 */
	public void setClientLogin(ClientLogin loginObj) {
        //append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] connected to server\n"
                + "\"" + serverName  + "\"\n");
        chatLog.append("*********************************************\n");
		this.clientLoginObject = loginObj;
	}

	/**
	 * sets name of server we are logged into
	 * @param serverName
	 */
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }
    
    public void logSuccessfulAccountCreate(String accountName)
    {
        //append event to chatlog
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String timeString = sdf.format(now);
        chatLog.append("[" + timeString + "] successfully created\n"
                + "account \"" + accountName  + "\"!\n");
    }

    private void displayErrorMessage (String message)
    {
        //JOptionPane.showMessageDialog (f, message, "Error", 
        //        JOptionPane.ERROR_MESSAGE);
        ShowErrorNonBlocking.showError(message);
    }




    
    


}

// vim: ts=4 sw=4 et
