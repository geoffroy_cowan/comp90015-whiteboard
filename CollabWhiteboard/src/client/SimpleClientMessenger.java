package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SimpleClientMessenger extends UnicastRemoteObject
    implements ClientMessenger
{
    
    private static final long serialVersionUID = -5110748123903898097L;
    private ClientReceiver receiver;

    /*
     * needs to maintain reference to ClientReceiver to relay
     * communications to
     */
    public SimpleClientMessenger(ClientReceiver receiver)
            throws RemoteException
    {
        super();
        this.receiver = receiver;
    }

    /*
     * These methods all just relay to the corresponding ClientReceiver
     * method
     */
    public void relayBoardUpdate(BoardModification modification,
                String updatingClient)
            throws RemoteException
    {
        receiver.provideBoardUpdate(modification, updatingClient);
    }

    public void relayWarnInactiveLogout(int secsToLogout)
            throws RemoteException
    {
        receiver.warnInactiveLogout(secsToLogout);
    }

    public void relayAlertClientLogin(String newClientName)
            throws RemoteException
    {
        receiver.alertClientLogin(newClientName);
    }

    public void relayAlertClientLogout(String oldClientName)
            throws RemoteException
    {
        receiver.alertClientLogout(oldClientName);
    }

    public void relaySetImage(BoardImage image)
            throws RemoteException
    {
        receiver.setBoardImage(image);
    }
    
    public boolean relayAskLoginPermission(String newClientName)
    {
        return receiver.askLoginPermission(newClientName);
    }

    public void relaySetNewAdmin(String newAdminName) throws RemoteException
    {
        receiver.setNewAdmin(newAdminName);
    }
    
    public BoardImage relayGetCurrentImage() throws RemoteException
    {
        return receiver.getCurrentImage();
    }
    
    public void relayAlertKick() throws RemoteException
    {
        receiver.alertKick();
    }

}
