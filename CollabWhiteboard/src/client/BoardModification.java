package client;

import java.io.Serializable;

/**
 * An interface for board modifications; such objects should always be
 * serializable.
 *
 * This interface provides a framework for clients to describe a
 * modification that they have made to the whiteboard in a way that can be
 * relayed to other clients.
 * 
 * Server side code assumes that any object used to represent a modification
 * is an implementation of this interface
 */
public interface BoardModification extends Serializable
{
    //add declarations of any methods that should be implemented
    //e.g. a method to obtain list of pixel coordinates altered by the
    //modification might by useful
}
