package client;

import javax.swing.JOptionPane;

/**
 * Creates an error dialogue without blocking until user interacts with it
 */
public class ShowErrorNonBlocking implements Runnable
{
    private String message;
    public ShowErrorNonBlocking(String message)
    {
        this.message = message;
    }

    /*
     * factory method for constructing then running thread
     */
    public static void showError(String message)
    {
        ShowErrorNonBlocking errorObject = new ShowErrorNonBlocking(message);
        Thread threadToRun = new Thread(errorObject);
        threadToRun.start();
    }
    
    public void run()
    {
        //JFrame errorFrame = new JFrame("Error frame");
        JOptionPane.showMessageDialog (null, message, "Error", 
                JOptionPane.ERROR_MESSAGE);
    }

}
