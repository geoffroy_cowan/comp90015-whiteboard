#!/bin/bash
#boots the server on localhost using the supplied command line argument
#as port number this script attepts to start the RMI registry and the java
#server program using the supplied port number

#NOTE: this assumes the java program is already compiled, and that we
#have supplied a valid port number (if not supplied, default to 1099)

num_args=$#
if (( "$num_args" == 1 )) ; then
    port=1099
    hostn=$1
elif (( "$num_args" == 2 )); then
    port=$2
    hostn=$1
else
    echo "Error: should have at most 1 command line argument"
    echo "(hostname of server, and optionally port number)"
    exit
fi

# tell java where to find our compiled *.class files.
export CLASSPATH="./bin:${CLASSPATH}"

echo "Attempting to boot rmiregistry and whiteboard server on port "$port
rmiregistry $port &
java -Djava.security.manager -Djava.security.policy=allPermissions.policy server.SimpleBootServer $port $hostn

# stop the RMI registry. This is important, because if we don't kill it
# (well, SIGTERM, not kill -9), it will remain listening on whatever socket
# it's using, and we won't be able to rerun this script until it has been
# terminated.
pkill rmiregistry
