#!/bin/bash
#easy script to start client, no command line arguments required

num_args=$#
if (( "$num_args" == 1 )) ; then
    hostn=$1
else
    echo "Error: should have just 1 command line argument"
    echo "(hostname of client, by which server can contact client)"
    exit
fi


export CLASSPATH="./bin:${CLASSPATH}"
java -Djava.security.manager -Djava.security.policy=allPermissions.policy client.ClientMain $hostn &
