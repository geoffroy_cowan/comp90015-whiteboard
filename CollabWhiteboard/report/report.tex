\documentclass[a4paper]{article}

\title{COMP90015 Project 2 -- Shared Whiteboard}
\author{A. Bennet \and G. Cowan \and D. Gray \and M. Signorini}

\usepackage{graphicx}

% make hypertext links in the pdf file. This has to be here at the end of
% the preamble, because it does some voodoo.
\usepackage[pdftex,bookmarks,colorlinks]{hyperref}

\begin{document}

\maketitle
\tableofcontents
\newpage


\section{Introduction}
A shared whiteboard is form of online collaboration where multiple users 
are able to make various modifications to a shared graphic in real time. 
Such a system requires the ability to modify shared objects in a remote 
setting, and handling multiple users and sessions simultaneously. The 
quality of server, session, and user management will influence the success 
of the system.

Our software allows multiple collaborators to draw and edit shapes/lines of different colors, strokes and fill settings, and group chat across a single shared whiteboard. User management requires an account and valid credentials to create or join a whiteboard and enforces a single admin for each whiteboard instance. A whiteboard session may be joined through an invitation or permission from the admin or logging into a whiteboard where permission had been previously granted by an admin. Additionally admins have the ability to terminate and kick users from a whiteboard session. If an admin leaves, the next user to have joined the whiteboard will be automatically promoted to admin. A list of active users and the current admin is displayed and updated regularly when connected to a whiteboard.

Connection to a whiteboard server is required before logging in. A server is capable of hosting multiple shared whiteboard instances which a user can select to join. Session management is maintained by deleting whiteboard sessions with no active clients. At any given time a current user of a session may save the whiteboard as a *.png file on their local computer.

\section{System Architecture}
In this section we discuss the overall architecture of our solution to the 
distributed whiteboard problem. Figure~\ref{basic} shows a simple block diagram, 
illustrating the main components of our architecture and how they relate to 
each other. The architecture is broken into two disjoint components, the 
server architecture, and the client architecture.
\begin{figure}
    \begin{center}
        \includegraphics[width=5cm,height=7cm]{./diagrams/basic.png}
    \end{center}
    \caption{A high level overview of the system\label{basic}}
\end{figure}
The client consists of three main components:
\begin{description}
    \item[Connection management component:] interacts with the central 
        session management component of server. This component is 
        responsible for connecting to a server and obtaining references to 
        other server components, to be used by other parts of client. 

    \item[GUI component:] provides a canvas for the user to draw on. This 
        component interacts with a single whiteboard instance on server. 

    \item[Account setup component:] provides a facility for creating user 
        accounts (discussed in more detail in next section). This 
        component interacts with the account management component of the 
        server. 
\end{description}
Similarly, the server consists of three major components:
\begin{description}
    \item[Session management component:] the main point of contact in 
        server for new clients which also has access to all other parts of 
        server. 

    \item[Account management component:] responsible for maintaining user 
        account information, and authenticating login attempts. 

    \item[Whiteboard sessions:] A set of zero or more whiteboard instances, 
        each of which has one or more logged in clients that it interacts 
        with (via the GUI component of each client). 
\end{description}

At a high level, the work flow of our system for a new client wishing to 
use a whiteboard can be viewed as the following:
\begin{enumerate}
    \item The new uses connection management component of client to 
        connect to server, and obtain reference to Account Management 
        component. 

    \item User sets account on server using account setup component of 
        client. 

    \item User authenticates via connection management, and logs into a 
        whiteboard instance (either an existing instance, in which case 
        administrator of that instance must give permission, or a new 
        instance). 

    \item Now user is logged into a whiteboard instance, and all 
        modifications to their canvas are transmitted to canvases of all 
        other clients logged in to that instance (and vice versa). 
\end{enumerate}
Returning users already have an account, and thus can skip straight to step three above.

It is important to emphasise in outlining our architecture that in our design, for a single server there exist multiple (0 or more) independent whiteboard instances on that server, with multiple (1 or more) clients connected to each of those whiteboard instances. An individual client can only be logged in to a single whiteboard instance on a single server at a time. In addition, the server is not necessarily located on the same machine as any of the clients.


\section{Design of the Shared Whiteboard}

\subsection{Communication Protocol}
Given we were implementing our solution in Java, we decided to use Java’s RMI framework for remote communication. This decision was based on the fact that both client and server would be implemented in Java, and thus remote classes could easily be created to provide synchronized services to remote clients/servers. Since RMI does not have to deal with non-Java API’s the implementation of remote components is thus greatly simplified.
The main drawback of this decision is that, because RMI is a closed protocol,
our system's dependency on RMI prevents the development of alternative
clients or servers by other organisations, and also ties our system to the
Java programming language. However, we consider the advantages of RMI's
simplicity to outweigh these disadvantages.

\subsection{Security Considerations}
% TODO: overview paragraph.
We do not rely on complex objects that are downloaded via remote method calls. Thus the methods called on all such objects require minimal permission, and our system can function with very restrictive RMI security policies. Therefore this allows users/servers who are working in a potentially unsafe environment to prevent the execution of dangerous downloaded byte-code, without restricting the functionality of client/server.

We provide users of the system with a protected identity, by requiring the setup of a user account (with username and password), and requiring them to supply correct password in order to log in to a given whiteboard instance. Thus, assuming passwords are protected, a user cannot mimic another user.

We allow each whiteboard instance to have a single administrator, who is able to control access to that instance. When a new user attempts to log in to an instance, they must receive permission from the administrator. Thus, together with protected identities, this allows a user who sets up a new whiteboard instance to restrict those who log in to that board to known friends/colleagues, and prevent unknown users from causing damage. Furthermore, certain operations can only be performed by the administrator.

We provide security in the transmission of passwords through remote method calls via cryptography. We use public key based cryptography (using 4096 bit RSA), in order to encrypt password to be sent when creating a new user account, and message-digest based cryptography (using SHA-1) in order to hash passwords that are sent for login attempts (the use of the latter, rather than RSA, for logging in preventing attacks based on observing and re-using encrypted credentials)

\subsection{GUI based Client for Whiteboard Users}
Given the visual nature of a shared whiteboard system, it is clear that the
client side application must provide a graphical interface to the user,
allowing them to view their modifications to the whiteboard. We have
implemented a GUI using the Java Swing and AWT frameworks, and our client
displays a window with a canvas for drawing on as well as other user
interface components, such as menus, and a toolbar. The menus provide
network functionality, such as choosing a server to connect to by specifying
the host name and port number; administrator functions such as evicting a
user at the administrators discretion; loading and saving whiteboards to the
local machine, using the PNG graphics file format and so on.

We have also implemented an instant messaging system that allows users who
are logged in to the same whiteboard session to communicate amongst
themselves, allowing users to coordinate their drawing activities.
All events, such as other clients logging in or out, are also logged in the
messaging pane, and our client application also displays a list of all the
users logged into the whiteboard session with the session administrator in
bold text.

We designed the client to ensure that all users will have consistent whiteboard
screens at all times, even when two or more users attempt to draw on the
same region of the whiteboard at exactly the same time. This is done by only
drawing changes onto the canvas in the order that we receive them from the
server, including for changes made by the local user, for whom the server
echos the modification message for this exact purpose. This technique allows
the server to enforce a linear order of events, effectively defining which
drawing event occurrs first when two such events happen simultaneously.

Our system supports drawing several different types of objects, such as
free hand or straight line drawing, ellipses and rectangles, either filled
or unfilled, and writing text onto the canvas. The paintbrush thickness and
colour can both be customised.


% TODO: Class diagram figure in this section
\section{Implementation of the Shared Whiteboard}
\begin{figure}
    \begin{center}
        \includegraphics[width=10cm,height=8cm]{./diagrams/connection-and-account.png}
    \end{center}
    \caption{An interaction diagram displaying the sequence of calls involved
    in creating a new account, or establishing a connection.\label{newca}}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=7cm,height=7cm]{./diagrams/blockdiagram.png}
    \end{center}
    \caption{Block diagram of the components of the server and the client, showing
    how they interact with one another.\label{bd}}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=9cm,height=12cm]{./diagrams/class-diagram.png}
    \end{center}
    \caption{High level overview of the class diagram, showing only the most
    important components.\label{cd}}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=11cm,height=6cm]{./diagrams/modifications.png}
    \end{center}
    \caption{Interection diagram illustrating the sequence of calls that occurr
    when a user makes a modification to the whiteboard.\label{mods}}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=10cm,height=6cm]{./diagrams/user-login.png}
    \end{center}
    \caption{Interaction diagram showing the method calls involved when a user
    logs into a whiteboard session.\label{login}}
\end{figure}

Each client is identified by a ClientReceiver (implemented by ClientMain) instance and provided with a ClientGUI to access and perform all the functions of the shared whiteboard system. The ClientReceiver (implemented by SimpleClientReceiver) receives user management and board updates from a ClientMessenger (implemented by SimpleClientMessenger). Incoming information to the Receiver is visualised through the Receiver’s GUI.

The ClientGUI allows a user to modify the whiteboard through a range of Tools including shapes, freedraw and text, with different colors and fill settings. Each change to the whiteboard is encapsulated within a BoardModification (implemented by PenMod, OvalMod, RectMod, TextMod and ChatMod) that contains all the information to replicate the change on another whiteboard, thus providing a suitable format (serialisable) to relay whiteboard changes to other clients. Group chat shares the same communication mechanism as board modifications, but is presented separately from the shared whiteboard graphic.

A client must obtain reference to the server RMI registry before creating an account and connecting to a whiteboard. The class SetupConnectionDetailsDialogue creates a new ClientLogin object which gets the RMI registry by serverName and port number and obtains a RemoteLogin login object from the server by looking up the loginServerName in the registrty. The class RemoteLogin (implemented by SimpleRemoteLogin) exports this remote object from the server in order to handle basic, pre login requests. The class ClientLogin (implemented by SimpleClientLogin) provides the framework for logging into a remote whiteboard instance on the server. We see that ClientLogin is a client-side class that stores reference to the server-side RemoteLogin class.

Setting up a new connection also involves calling ClientLogin.registerReceiver(receiver) which assigns a new ClientMessenger for the Receiver. It makes sense that establishing a new receiver requires creating a channel (the messenger) for the receiver to receive information. ClientMessenger lives on the client machine and accept remote calls, via RMI, from the server machine and makes use of the methods defined by the ClientReceiver interface to inform the client of the various events. We do this from this ClientMessenger rather than directly from ClientServant so that we do not have to serialize and store a copy of the ClientReceiver on the server; instead we can just keep a reference to this on server; makeing for lightweight, two-way communication.

Creating an account involves using the AccountCreateDialogue to register a username and password (stored in AccountDetails) at ClientLogin, then creating an account on the server through RemoteLogin. All client/server login information is encrypted before sending. 

Joining an existing whiteboard session involves using the WhiteboardSelectionDialogue to select a session from a session list built from the HashSet of alive whiteboard instances on RemoteLogin. Username and password authentication is required before joining a session which involves registering ClientCredentials at ClientLogin using a PasswordDigest. ClientCredentials is a class to encapsulate the information that a client must provide to the server in order to authenticate, and PasswordDigest computes a cryptographic hash of a password and a salt. CryptographicSalt provides an abstraction for a cryptographic salt. This is logically a random sequence of bytes which is appended to a secret token (password) before hashing the whole sequence in a digest based authentication scheme. Issuing different salts for each authentication attempt prevents replay attacks, when an attacker who does not know the password repeats the intercepted transmissions of a genuine client.

Credentials are stored in a PasswordMap that provides an abstract mapping from a username to a password that is stored on the server, and implements mapping by keeping a set of files, one per user, that store the password for the corresponding user.

Successful authentication provides the ClientGUI with a ClientServant object which is linked to the CentralWhiteboard on the server. The CentralWhiteboard (implemented by SimpleCentralWhiteboard) receives update calls through remote Servant objects, and ultimately relays them to Receiver objects of all other clients in the whiteboard session. ClientServant (implemented by SimpleClientServant) is a Remote servant class that lives on the server and extends UnicastRemoteObject. Hence ClientServant is the stub for a remote object exported by CentralWhiteboard which holds the skeleton object BoardImage (implemented by WhiteboardImage).

User management methods are implemented across the Receiver, GUI, Servant, CentralWhiteboard, and Messenger classes. The GUI and the CentralWhiteboard require up-to-date client/admin information for each whiteboard session, and include invite/kick, promote/approve, login/logoff methods to relay the current client status. BoardModifications (including group chat) and user management methods share the same communication mechanism where the actions are passed from the GUI to CentralWhiteboard, then a messagetype is sent to all clients in the session. Different message types prompt different actions that include the client making board modifications, logging in/off, being kicked or promoted as a new admin, or setting a new whiteboard image. Actions involving a particular client, such as being kicked from the session prompt a screen alert in their GUI.

Session management is handled by CentralWhiteboard where every session requires at least one admin (one active user), otherwise the session will terminate. If an admin leaves, the next client in the whiteboard session is automatically promoted as admin. The CentralWhiteboard pokes and warns clients that inactivity will lead to them being kicked from the session after a set timeout period, hence CentralWhiteboard maintains a list of active clients only.

\section{Critical Evaluation}

\subsection{Correctness}
Our client design, as outlined above, enforces a consistent ordering of all actions applied to the “shared” whiteboard between all clients logged in to the same whiteboard instance. Thus in terms of synchronicity, our solution is guaranteed to behave correctly.

In addition, synchronization of methods in the client’s GUI environment is controlled to prevent deadlock situations from arising. This ensures liveliness of our client.

\subsection{Responsiveness}
The responsiveness of our client is heavily dependant on network latency. Because all updates user makes to board are sent to server then echoed back, to ensure correctness (as described above), there can be some lag between drawing and receiving feedback if they are operating on a poor network.

We investigated alternative solutions, such as drawing own updates directly on to board, and re-fetching a new blank image if update was detected to have been made in wrong order. We found that this alternative implementation performed better in situations when a single client was drawing (for obvious reasons avoiding network latency), but performed worse in situations when multiple clients were drawing at once (as updates were constantly being made out of sync requiring frequent requests for images from server). Thus given that our task was to create a system that performs well for multiple users simultaneously, we found that our chosen solution produced the best results in terms of performance overall in such heavy-use scenarios.

In addition, our finely grained synchronization on server allows responsiveness to be maintained in scenarios when server is waiting a long time for administrator responses (i.e. when new user requesting to join board), by preventing the central whiteboard instance from freezing until they respond. This ensures our client maintains its level of responsiveness under all conditions.

\subsection{Scalabillity}
Our solution is purposefully designed to be highly scalable, with multiple whiteboard instances able to run simultaneously from a single server. Following this paradigm, our solution could easily be extended with little effort to run the whiteboard sessions for a single server across multiple machines, using a naming service to access one of several coordinating servers. This means our framework could easily be extended to fit in a very large scale, enterprise environment.

In addition, because the server is extremely light-weight (takes no responsibility in doing anything with board updates; simply acts as a central coordinator to forward updates to everyone in a synchronized order), it occupies very little resources on the server’s local machine, and thus can easily support a large number of simultaneous whiteboard sessions.

Further, we have created our user account system in a way that is highly scalable, storing/accessing user account information from server file-system, rather than from an in-memory data structure, which allows the system to scale well under large numbers of user accounts.

\subsection{Security Features}
By our design (as outlined above), our solution provides strong security, in terms of minimal permissions required by remote code, protected identity of users, encryption of passwords sent over networks, and ability of whiteboard administrators to protect their whiteboard session from rogue users.

In addition to these security design elements, in practice our server provides additional security by forcing all updates to go through remote ClientServant objects, which allows server to prevent updates that should be illegal but are subverted by rogue users using a modified client program (for instance, modifying client to prevent check of administrator privileges before trying to kick a user).

One security flaw in our system is that public keys for encryption are obtained by remote method call (thus potentially exposing client to “man in middle” attack). An enterprise version of our system could easily be extended to instead use some kind of certificate based system to obtain public key from a trusted source, however we felt this was outside the scope of this project.

\subsection{User Friendly Features}
The client application's user interface has been designed with usability in
mind, and as such it has the following features:
\begin{itemize}
    \item Clear dialogues for performing network operations, with helpful error messages if the user tries to perform an invalid operation, such as trying to login to a whiteboard before connecting to server.

    \item All events are logged in a clear way in chat panel, so the user can easily see what has been going on in terms of server events. This also provides clear feedback from operations (e.g. recording success after login, and displaying name of logged in to server)

    \item Chat feature allows new users to gain help from more experienced users.

    \item Enforced restrictiveness against invalid actions provides robustness, preventing the user from accidentally crashing the client.
\end{itemize}


\section{Concluding Remarks}
In this project, we have successfully implemented a shared whiteboard written in the Java programming language, making use of Java's Remote Method Invocation framework to simplify communications between the client and the server. We have implemented a window based client application, with several features, such as an instant messaging component, allowing users to communicate amongst themselves, and intuitive menus and dialogs which are used to connect to a whiteboard server and join a whiteboard hosted by that server. Our system also allows users to draw lines, shapes and text on the whiteboard in a range of colours and sizes.

By adopting a ``thick client'' model, whereby the server's role is simply to relay modifications made by any given client to all the other clients who are members of that session, our server is highly scalable, and will be able to host many whiteboard sessions with reasonable resource requirements. By allowing whiteboard admins to manage the whiteboard via an ordinary client application, our system is also well suited for cloud deployment, where the whiteboard may be provided as a service, and users of that service do not have access to the computers hosting it to start applications. This model of deployment is also better suited to access over the internet, when the whiteboard administrator may not have a domain name, or may not be able to run the server on their own machine for whatever reason (such as resource constraints).

Our system also makes use of cryptographic techniques to ensure that its user’s account details are kept secure. We decided to allow users to create their own account via the client application, similar to free sign up on various other web services. This requires the client to send the password for the new account to the server, which is done using RSA encryption to ensure that the new password is not revealed to potential eavesdroppers. When users sign in, their password is verified using a challenge-response scheme, such that the password is not sent over the network. The client instead sends a hash of their password, with a cryptographic salt string appended to it. The difficulty of reversing the hash function means that an evesdropper could listen in, but would not be able to determine the password that was verified by the challenge-response handshake.

% TODO: maybe have this in an appendix? probably don't want many figures
% in an appendix though, because the reader then has to search for them.
\section{Team Member Contributions}
\begin{description}
    \item[Andrew] network architecture and implementation, report (system architecture, design - components, evaluation)
    \item[Geoff] Network implementation, whiteboard functions, board updates, general GUI
    \item[Matt] account management, network encryption and authentication, menu GUI, report (conclusion, latex formatting)
    \item[Daniel] board modifications, effects, button GUI, report (intro, system design - classes, diagrams - block, class, interaction)
\end{description}

\end{document}

% vim: ts=4 sw=4 et
